#include "integrals.hpp"
#include "globalvars.hpp"
#include "mathutilities.hpp"

void soad(std::string basis_set, std::vector<Atom>& atoms, 
        std::vector<double>& TV,
        std::vector<int>& translateS, std::vector<int>& translateL,
        std::vector<Eigen::MatrixXcd>& xD_master){


       /**********************************
       *
       *  Perform Hartree-Fock on Each 
       *  atom.  Block out density matrix
       *     Turned off parallelism
       *
       * *******************************/
 

  std::cout << basis_set << std::endl;
 
  BasisSet obs_master(basis_set, atoms);
  size_t nao_master = 0;
  for (auto s=0; s<obs_master.size(); ++s)
    nao_master += obs_master[s].size();
  
  int basis_position = 0;
  for(int i = 0; i < atoms.size(); i++){ //Master loop over atoms


      std::cout << "atom with atomic number " << atoms[i].atomic_number << std::endl;
      std::vector<Atom> tmp; //reinitializes tmp each loop
      tmp.push_back(atoms[i]);
      BasisSet obs(basis_set, tmp);
      std::cout << "Number of basis functions on this atom " << obs.nbf() << std::endl;
      size_t nao = 0;
      for (auto s=0; s<obs.size(); ++s)
        nao += obs[s].size();

      int nocc = 0;
      std::cout << "Confirming initialized nocc " << nocc << "\n";
      for(auto i = 0; i < tmp.size(); i++){
          nocc += tmp[i].atomic_number;
      }

      //forces closed shell species...its okay to have a little extra repulsion
      //for the first iteration of Hartree-Fock
      std::cout << "Confirming nocc now atomic_number " << nocc << " " << tmp[0].atomic_number << "\n";
      if (nocc%2 == 0) {
             ;
      } else {
        nocc += 1;
      }
      nocc = 0.5*nocc;
      std::cout << "Occupied Atomic Orbitals " << nocc << "\n";
  
      /**********************************
       *
       * get one electron quantities
       *
       *     Turned off parallelism
       *
       * *******************************/
      std::vector<Matrix> Overlaps; Overlaps.resize(2*NAMUR_S + 1);


//#if defined(_OPENMP)
//#pragma omp parallel for
//#endif
    for(int i = 0; i < translateS.size(); ++i){
      auto S = compute_1body_ints(obs, libint2::OneBodyEngine::overlap, TV, translateS[i]);
      Overlaps[i] = S;
    }
    std::vector<Matrix> Kinetic; Kinetic.resize(2*NAMUR_S + 1);
//#if defined(_OPENMP)
//#pragma omp parallel for
//#endif
        for(int i = 0; i < translateS.size(); ++i){
          auto T = compute_1body_ints(obs, libint2::OneBodyEngine::kinetic, TV, translateS[i]);
          Kinetic[i] = T;
          
        }
        std::vector<Matrix> Potential; Potential.resize(2*NAMUR_S + 1);
//#if defined(_OPENMP)
//#pragma omp parallel for
//#endif
        for(int i = 0; i < translateS.size(); ++i){
          Matrix V = compute_1body_ints(obs, libint2::OneBodyEngine::nuclear, TV, translateS[i], atoms);
          Potential[i] = V;
        }



       /**********************************
       *
       *   Two electron quantities
       *
       *     Turned off parallelism
       *
       * *******************************/
        std::vector<double> timings;
        std::vector<Matrix> TwoInts;
        TwoInts.resize((2*(NAMUR_S+NAMUR_L)+1)*(2*NAMUR_L+1)*(2*(NAMUR_S+NAMUR_L)+1));
        timings.resize((2*(NAMUR_S+NAMUR_L)+1)*(2*NAMUR_L+1)*(2*(NAMUR_S+NAMUR_L)+1));
        std::cout << "Allocated two Electron integral matrix\n\n"; 
        int count = 0;
        for(int j = -1*(NAMUR_S+NAMUR_L); j < (NAMUR_S+NAMUR_L)+1; j++){
            for(int m = -1*NAMUR_L; m < NAMUR_L+1; m++){
                for(int n = -1*(NAMUR_S+NAMUR_L); n < (NAMUR_S+NAMUR_L)+1; n++){
                    int idx = (j+(NAMUR_S+NAMUR_L))*((2*NAMUR_L+1)*(2*(NAMUR_L+NAMUR_S)+1)) + (m+NAMUR_L)*(2*(NAMUR_S+NAMUR_L)+1) + (n+NAMUR_S+NAMUR_L);

        const auto tstart = std::chrono::high_resolution_clock::now();

                    TwoInts[idx] = compute_2body_ints(obs, j, m, n, nao,  TV, tmp);
 
      const auto tstop = std::chrono::high_resolution_clock::now();
      const std::chrono::duration<double> time_elapsed = tstop - tstart;
                    timings[idx] = time_elapsed.count();
 
                    count += 1;
                }
            }
        }


    /*** =========================== ***/
    /*** Fourier Transform of        ***/
    /*** one-electron quantities     ***/
    /*** =========================== ***/


    std::vector<Eigen::MatrixXcd> KOverlaps;
    std::vector<Eigen::MatrixXcd> KKinetics;
    std::vector<Eigen::MatrixXcd> KPotentials;

    KOverlaps.resize(K_L);
    KKinetics.resize(K_L);
    KPotentials.resize(K_L);
    std::cout << "Allocated K-space 1-electron properties \n";

    double a = double(TV[2]);
    std::vector<double> momentum = { 0.0, 0.0, 0.0};
    std::vector<double> kvec;
    kvec.resize(K_L);
    for(auto i = 0; i != K_L; i++){
        momentum[2] = (2*M_PI/a)*(double(i)/double(K_L));
        kvec[i] = momentum[2];
        KOverlaps[i] = one_fourier(Overlaps,momentum,translateS, a);
        KKinetics[i] = one_fourier(Kinetic,momentum,translateS, a);
        KPotentials[i] = one_fourier(Potential, momentum, translateS, a);
    }

    std::vector<Eigen::MatrixXcd> Xs;
    Xs.resize(K_L);

    /*** ============================ ***/
    /*** Orthogonalization of basis   ***/
    /*** functions(. Had problem with ***/
    /*** canonical orthogonalization. ***/
    /*** using symmetric.)            ***/
    /*** ============================ ***/

    for(auto i = 0; i < K_L; i++){
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> diag(KOverlaps[i]);
 
        Eigen::MatrixXcd ssqrt = Eigen::MatrixXcd::Zero(nao, nao );
        for(auto ii = 0; ii < nao; ii++){
          ssqrt(ii,ii) = 1./sqrt(diag.eigenvalues()(ii) );
        }
        Xs[i] =  diag.eigenvectors()*ssqrt ;

    }
    std::cout << "Got overlaps \n";



    /*** =========================== ***/
    /*** Generate Guess from Core    ***/
    /*** Matrix                      ***/
    /*** =========================== ***/

    std::vector<Eigen::MatrixXcd> KF_prime;
    KF_prime.reserve(K_L);
    for(auto i = 0; i < K_L; i++){
        KF_prime.push_back(Xs[i].adjoint()*(KKinetics[i]+KPotentials[i])*Xs[i]);
    }

    std::vector<e_ev_pair> EigenPairs;
    EigenPairs.reserve(nao*K_L);
    double kval_temp = 0.0;
    Eigen::MatrixXcd C;
    for(auto i = 0; i < KF_prime.size(); i++){
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> diag(KF_prime[i]);
        C = Xs[i]*diag.eigenvectors();
        kval_temp = (2*M_PI/a)*double(i)/(double(K_L));
        for(auto j = 0; j < diag.eigenvalues().size(); j++){
           EigenPairs.push_back(e_ev_pair(i, kval_temp, diag.eigenvalues()(j), C.col(j) ));
        }

    }
    std::cout << "Got eigen pairsy \n";


    std::sort( EigenPairs.begin(), EigenPairs.end() );
    std::vector<Eigen::MatrixXcd> KDensity;
    KDensity.resize(K_L);
    for(auto i = 0; i < (K_L); i++){
        KDensity[i] = Eigen::MatrixXcd::Zero(nao,nao);
    }
    std::cout << "Zeroed KDensity \n";
    std::cout << "nocc " << nocc << "\n";
    for(auto i = 0; i < (K_L*nocc); i++){
        KDensity[EigenPairs[i].idx] += 2.*EigenPairs[i].eigvec*(EigenPairs[i].eigvec.adjoint());

    }
    std::cout << "Allocated Kdensity \n";

    /*** =========================== ***/
    /*** Generate Density from       ***/
    /*** eigen decompostion of guess ***/
    /*** fock matrix                 ***/
    /*** =========================== ***/

    std::vector<Eigen::MatrixXcd> xD;
    xD.reserve(2*NAMUR_S+1);
    std::complex<double> ii(0,1);
    for(int i = 0; i < translateS.size(); i++){
        Eigen::MatrixXcd tmp = Eigen::MatrixXcd::Zero(nao,nao);
        for(int k = 0; k < K_L; k++){
            tmp += KDensity[k]*exp(ii*kvec[k]*double(a)*double(translateS[i]));
        }
        xD.push_back(tmp/double(K_L));
    }


    double Norm = 0.0;
    for(int i = 0; i < xD.size(); i++){
        Norm +=  (xD[i]*Overlaps[i]).trace().real();
    }
    std::cout << "Norm = " << Norm/2 << "  nocc = " << nocc << "\n";


    //start scf prep
    const auto maxiter = 100;
    const auto conv = 1e-12;
    auto iter = 0;
    auto rms = 0.0;
    auto ediff = 0.0;
    auto ehf = 0.0;
    auto oneE = 0.0;
    auto twoE = 0.0;
    //double kenergy = 0;
    //double koneenergy = 0;

    Eigen::MatrixXd KpointEigs(nao, K_L);
    std::vector<Eigen::MatrixXcd> xD_last;
    xD_last.reserve(xD.size());
    std::vector<Eigen::MatrixXcd> Coeffs;
    //initialize the coeffs
    for(int i(0); i < K_L; i++){
        Coeffs.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }
    std::vector<Eigen::MatrixXcd> kDensityMO;
    //initialize the coeffs
    for(int i(0); i < K_L; i++){
        kDensityMO.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }

    for(auto i = 0; i < 2*NAMUR_S+1; i++){
        xD_last.push_back(xD[i]);
    }


    //Get everything ready for SCF procedure
    std::vector<Matrix> Fprime;
    Fprime.reserve(2*NAMUR_S+1);
    for(auto i = 0; i < 2*NAMUR_S+1; i++){
      Fprime.push_back(Matrix::Zero(nao,nao));
    }
    std::vector<Eigen::MatrixXcd> KF;
    KF.reserve(K_L);
    for(auto i = 0; i < K_L; i++){
      KF.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }
    std::vector<Eigen::MatrixXcd> Gmat;
    Gmat.reserve(2*NAMUR_S+1);
    for(auto i = 0; i < 2*NAMUR_S+1; i++){
        Gmat.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }
    std::vector<Eigen::MatrixXcd> J2;
    J2.reserve(2*NAMUR_S+1);
    for(auto i = 0; i < 2*NAMUR_S+1; i++){
        J2.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }
    std::vector<Eigen::MatrixXcd> X2;
    X2.reserve(2*NAMUR_S+1);
    for(auto i = 0; i < 2*NAMUR_S+1; i++){
        X2.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }



    std::vector<Eigen::MatrixXcd> tGmat;
    tGmat.reserve(2*NAMUR_S + 1);
    for(auto i = 0; i < 2*NAMUR_S + 1; i++){
        tGmat.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }
    std::vector<Eigen::MatrixXcd> Hamiltonian;
    Hamiltonian.reserve(2*NAMUR_S + 1);
    for(auto i = 0; i < 2*NAMUR_S + 1; i++){
        Hamiltonian.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }
    std::vector<Eigen::MatrixXcd> KHamiltonian;
    Hamiltonian.reserve(K_L);
    for(auto i = 0; i < K_L; i++){
        KHamiltonian.push_back(Eigen::MatrixXcd::Zero(nao,nao));
    }


    do {
      const auto tstart = std::chrono::high_resolution_clock::now();

      for(auto i = 0; i < 2*NAMUR_S+1; i++){
          Gmat[i].block(0,0,nao,nao) << Eigen::MatrixXcd::Zero(nao,nao);;
      }
      for(auto i = 0; i < 2*NAMUR_S+1; i++){
          J2[i].block(0,0,nao,nao) << Eigen::MatrixXcd::Zero(nao,nao);;
      }
      for(auto i = 0; i < 2*NAMUR_S+1; i++){
          X2[i].block(0,0,nao,nao) << Eigen::MatrixXcd::Zero(nao,nao);;
      }
 
   
      if (cutoff_type.compare("CT") == 0){
      ConstructG_cellwise(xD, TwoInts, translateS, Gmat, nao, K_L);
      }

     if (cutoff_type.compare("Namur") == 0 ||
            cutoff_type.compare("Namur_2") == 0 ){
          ConstructG_Namur_2(xD, TwoInts, translateS, Gmat, J2, X2, nao, K_L);
      }



      for(auto i = 0; i < 2*NAMUR_S+1; i++){
        Fprime[i].block(0,0,nao,nao) << (Kinetic[i] + Potential[i] + Gmat[i].real()  );
      }


    //turn on symmeterization if needed
      for(auto i = 0; i < NAMUR_S; i++){
        Fprime[i].block(0,0,nao,nao) << (Fprime[i] + Fprime[-1*i + 2*NAMUR_S].transpose())/2.;
        //Fprime[i].block(0,0,nao,nao) << Fprime[i];
 
        Fprime[2*NAMUR_S - i].block(0,0,nao,nao) << Fprime[i].transpose();
      }


      for(auto i = 0; i != K_L; i++){
          momentum[2] = (2*M_PI/a)*(double(i)/double(K_L));
          KF_prime[i].block(0,0,nao,nao) << one_fourier(Fprime, momentum,translateS, a);     
      }

      for(auto i = 0; i < K_L; i++){
        KF[i].block(0,0,nao,nao)  << Xs[i].adjoint()*KF_prime[i]*Xs[i];
      }
        


    
    std::vector<e_ev_pair> EigenPairs;
    EigenPairs.reserve(K_L*nao);
    double kval_temp = 0.0;
    for(auto i = 0; i < KF.size(); i++){
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> diag(KF[i]);
        C = Xs[i]*diag.eigenvectors();
 
        Coeffs[i].block(0,0,nao,nao) << C;
        kval_temp = (2*M_PI/a)*double(i)/(double(K_L));
        for(auto j = 0; j < diag.eigenvalues().size(); j++){
           EigenPairs.push_back(e_ev_pair(i, kval_temp, diag.eigenvalues()(j), C.col(j) ));
        }
        KpointEigs.block(0,i, diag.eigenvalues().size(), 1) << diag.eigenvalues();
        
    }

    std::sort( EigenPairs.begin(), EigenPairs.end() );
    for(auto i = 0; i < (K_L); i++){
        KDensity[i] = Eigen::MatrixXcd::Zero(nao,nao);
    }
    for(auto i = 0; i < (K_L*nocc); i++){
        KDensity[EigenPairs[i].idx] += 2.*EigenPairs[i].eigvec*(EigenPairs[i].eigvec.adjoint());

    }
   


    /*** =========================== ***/
    /*** Generate Density from       ***/
    /*** eigen decompostion of guess ***/
    /*** fock matrix                 ***/
    /*** =========================== ***/

    std::complex<double> ii(0,1);
    for(int i = 0; i < translateS.size(); i++){
        Eigen::MatrixXcd tmp = Eigen::MatrixXcd::Zero(nao,nao);
        for(int k = 0; k < K_L; k++){
            tmp += KDensity[k]*exp(ii*kvec[k]*double(a)*double(translateS[i]));
        }
        xD[i] = tmp/double(K_L);
    }



      // compute HF energy
      ehf = 0.0;
      oneE = 0.0;
      twoE = 0.0;
      for(auto l = 0; l < xD.size(); l++){
        oneE += (xD[l]*(Kinetic[l]+Potential[l])).trace().real();     
        twoE += ((0.5*xD[l])*Gmat[l].real()).trace().real();
        for (auto i = 0; i < nao; i++){
          for (auto j = 0; j < nao; j++){
            ehf += 0.5*xD[l](j,i).real() * (2*(Kinetic[l] + Potential[l]) + Gmat[l].real() )(i,j) ;
          }
        }
      }

    Norm = 0.0;
    for(int i = 0; i < xD.size(); i++){
        Norm += (xD[i]*Overlaps[i]).trace().real();
    }

      auto ehf_last = ehf;
      // compute difference with last iteration
      ediff = ehf - ehf_last;
      rms = 0.0;
      for(auto i = 0; i < xD.size(); i++){
          rms += (xD_last[i].real() - xD[i].real()).norm();
      }

    
      const auto tstop = std::chrono::high_resolution_clock::now();
      const std::chrono::duration<double> time_elapsed = tstop - tstart;

      if (iter == 0)
        std::cout <<
        "\n\n Iter        1E(elec)              2E(elec)               (E(tot))             RMS(D)         Time(s)      Norm\n";
      printf(" %02d %20.12f %20.12f %20.12f %20.12e %10.5f %10.5f\n", iter, oneE, twoE,
             oneE + twoE , rms, time_elapsed.count(), Norm);


       // Save a copy of the energy and the density
       // Sets the appropriate xD_master block
      for(auto i = 0; i < 2*NAMUR_S+1;i++){
          xD_last[i].block(0,0,nao,nao)  << xD[i];
          xD_master[i].block(basis_position, basis_position, nao,nao) << xD[i];
      }



    ++iter;
    } while (((fabs(ediff) > conv) || (fabs(rms) > conv)) && (iter < maxiter));


    basis_position += obs.nbf();




    } //for loop over atoms


    

}

