#include "integrals.hpp"
#include "globalvars.hpp"
#include "mathutilities.hpp"

using namespace std;


void MOIntegrals(Matrix& KpointEigs, std::vector<Eigen::MatrixXcd>& xD, 
        std::vector<Eigen::MatrixXcd>& KDensity, std::vector<Eigen::MatrixXcd>& KKinetics,
        std::vector<Eigen::MatrixXcd>& KPotentials,  std::vector<Eigen::MatrixXcd>& KOverlaps,
        std::vector<Eigen::MatrixXcd>& Coeffs,  std::vector<double>& kvec, 
        const int nao, const double a, const double enuc, BasisSet& obs, std::vector<Atom>& atoms,  
        std::vector<double>& TV, std::vector<Eigen::MatrixXcd>& KMultiP,
        double EMPE)
{

  std::vector<int> translateS(2*NAMUR_S + 1); 
  std::iota(std::begin(translateS), std::end(translateS), -1*NAMUR_S);

  Eigen::IOFormat HeavyFmt_local(15, 0, ", ", ",\n", "[", "]", "[", "]");

  std::vector<Eigen::MatrixXcd> kDensityMO; kDensityMO.resize(K_L);
  AssignZeroList(kDensityMO, nao);


  /**************************************************************************
   *
   * Set up maps and matrices to all the basis we'll need for purification
   * and representation as a real problem.  Also do inverse which is no the
   * adjoint 
   *
   * ***********************************************************************/

  std::vector<Eigen::MatrixXcd> KAO2ints; //two-electron integral matrices in k-space. 1-matrix per k-point
  std::vector<gem> gems;
  std::vector<std::vector<gem>> tgems;
  std::vector<korb> orbs;
  std::map<int, std::tuple<int,int>> bas;
  std::map<std::tuple<int,int>, int> bas_rev;
  std::map<int, std::tuple<int,int>> onebas;
  std::map<std::tuple<int,int>, int> onebas_rev;
  //Build single-particle basis in for k-space
  for(int i = 0; i < K_L; i++){
    for (int j = 0; j < nao; j++){
      orbs.push_back(korb(j, i));
      bas[i*nao + j] = std::make_tuple(j,i);
      bas_rev[std::make_tuple(j,i)] = i*nao + j;
      //our convention from now on should involve k first then n second
      //I will look through code later to see what changing previously
      //defined bas will do.  For now I'll just make a new one.
      onebas[i*nao + j] = (i <= K_L/2) ? std::make_tuple(i,j) : std::make_tuple(i - K_L,j) ;
      onebas_rev[std::make_tuple(i,j)] = i*nao + j;
    }
  }

  //for(int i = 0; i < K_L*nao; i++){
  //    std::cout << "(" << orbs[i].kx << "," << orbs[i].x << ")\n";
  //}
  //exit(0);

  //set up maps and iterators
  int cnt = 0;

  int r = 0;
  int s = 0;


  /***************************************************************************
   *
   * block coefficient matrices so we can do rotations via matrix
   * multiplications instead of for loops
   *
   * ************************************************************************/

  Eigen::MatrixXcd coeffmat = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);
  for(int i = 0; i < K_L; i++){
    coeffmat.block(i*nao, i*nao, nao, nao) = Coeffs[i];
  }
  std::vector<Eigen::MatrixXcd> Ckronecker;
  Ckronecker.resize(K_L); //added for parallel
  tgems.resize(K_L);
  KAO2ints.resize(K_L);

  //get geminals, rotate 2-electron integrals from k-space to x-space 1 block at a time, generage
  //kronecker product of final coefficients for integral AO -> MO transform

  const auto tstart_m2 = std::chrono::high_resolution_clock::now();
 
//#if defined(_OPENMP)
//#pragma omp parallel for
//#endif
  for(int i = 0; i < kvec.size(); i++){
    //find geminal set that satisfy kvec[i]
    std::cout << kvec[i] << std::endl;
    std::vector<gem> tmp_basis;
    for(int p = 0; p < orbs.size(); p++){
      for(int q = 0; q < orbs.size(); q++){

         if (i == (orbs[p].kx + orbs[q].kx)%K_L){
             //not a thread safe operation on gems.
             //gems.push_back(gem(orbs[p].x,orbs[q].x,orbs[p].kx, orbs[q].kx));
             tmp_basis.push_back(gem(orbs[p].x,orbs[q].x,orbs[p].kx, orbs[q].kx));
             //std::cout << "(" << orbs[p].kx << "," << orbs[p].x << ") (" << orbs[q].kx << "," << orbs[q].x << ")\t(" << bas_rev[std::make_tuple(orbs[p].x, orbs[p].kx)] << "," << bas_rev[std::make_tuple(orbs[q].x, orbs[q].kx)] << ")\n";
         }
      }
    }
    //With this info we can construct the kronecker product of coefficient
    //matrix for 2-electron integral transformation performed blockwise.
    tgems[i] = tmp_basis;
    Eigen::MatrixXcd tmp_kronecker(nao*nao*K_L, nao*nao*K_L);
    tmp_kronecker.setZero();
    for( int x = 0; x < tmp_basis.size(); x++){
      for(int y = 0; y < tmp_basis.size(); y++){
        tmp_kronecker(x,y) = coeffmat( tmp_basis[x].kx*nao + tmp_basis[x].x  ,
        tmp_basis[y].kx*nao + tmp_basis[y].x) * coeffmat( tmp_basis[x].ky*nao + tmp_basis[x].y , 
                    tmp_basis[y].ky*nao + tmp_basis[y].y );
      }
    }


    Ckronecker[i] = tmp_kronecker;
    Eigen::MatrixXcd T(nao*nao*K_L, nao*nao*K_L);
    T.setZero();
    std::cout << "xAO -> kAO for k " << i+1 << " of " << kvec.size() << "\n";
    rotateAO2KAO2intsOne(T, kvec, tmp_basis, nao, double(a), K_L, kvec[i], obs, atoms, TV);
    KAO2ints[i] = T;

  }

  const auto tstop_m2 = std::chrono::high_resolution_clock::now();
  const std::chrono::duration<double> time_elapsed_2 = tstop_m2 - tstart_m2;
  cout << "Time xAO -> kAO Total Time " << time_elapsed_2.count() << "\n"; 

  //Genergy KDensityAO
  Eigen::MatrixXcd KDensityAO = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);
  Eigen::MatrixXcd KHoneAO = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);
  Eigen::MatrixXcd KS = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);
  Eigen::MatrixXcd CoeffsB = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);
  for(int i = 0; i < K_L; i++){
    KDensityAO.block(i*nao , i*nao, nao, nao) = KDensity[i];
    KHoneAO.block(i*nao, i*nao, nao,nao) = KKinetics[i] + KPotentials[i] + KMultiP[i];
    KS.block(i*nao, i*nao, nao,nao) = KOverlaps[i];
    CoeffsB.block(i*nao, i*nao, nao, nao) = Coeffs[i];
  }

  //////////////////////////////////////////////////////
  //This stuff is an important AO TO MO transformation
  //////////////////////////////////////////////////////
  //Generate KMO from transformation of AO Density matrix
  //Can rotate each k-block independently
  for(int i = 0; i < K_L; i++){
    kDensityMO[i] = Coeffs[i].adjoint()*KOverlaps[i]*KDensity[i]*KOverlaps[i]*Coeffs[i];
  }

  Eigen::MatrixXcd KMOMat = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);
  Eigen::MatrixXcd KMOHone = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);

  for(int i = 0; i < K_L; i++){
    KMOMat.block(i*nao, i*nao, nao, nao) = kDensityMO[i];
    KMOHone.block(i*nao, i*nao, nao,nao) =  Coeffs[i].adjoint()*(KKinetics[i]+KPotentials[i]+KMultiP[i])*Coeffs[i];
  }


  //transform 2 electron integrals into k-space MO
  std::vector<Eigen::MatrixXcd> KMO2Ints_2;
  KMO2Ints_2.resize(K_L);
  //rotate AO -> MO blockwise fast.
  cout << Ckronecker.size() << "\t" << KAO2ints.size() << endl;
//#if defined(_OPENMP)
//#pragma omp parallel for
//#endif
  //Build k-space MO two-electron integrals in the ordering given in the AO
  //basis. The ordering is important as it is necessary for spin and symmetry
  //adapting later on.
  for(int i = 0; i < Ckronecker.size(); i++){
    std::cout << "kAO -> kMO for k " << i << " of " << Ckronecker.size() << std::endl;
    std::cout << KAO2ints[i].rows() << Ckronecker[i].rows() << std::endl;
    KMO2Ints_2[i] = Ckronecker[i].adjoint()*KAO2ints[i]*Ckronecker[i] ;
  }

  std::cout << "Building Larger V2 for energy calculation in k-space" << std::endl;
  //Put KMO2Ints_2 back into large matrix for to do rotation to real
  //Eigen::MatrixXcd VV2 = Eigen::MatrixXcd::Zero(nao*K_L*nao*K_L , nao*K_L*nao*K_L);
  //Eigen::MatrixXcd VV2_AO = Eigen::MatrixXcd::Zero(nao*K_L*nao*K_L , nao*K_L*nao*K_L);
  //Eigen::MatrixXcd VV2_MO = Eigen::MatrixXcd::Zero(nao*K_L*nao*K_L , nao*K_L*nao*K_L);
 
  CSpMat VV2_sparse(nao*K_L*nao*K_L, nao*K_L*nao*K_L);
  std::cout << VV2_sparse.rows() << "\t" << VV2_sparse.cols() << std::endl;
  std::cout << nao*K_L*nao*K_L << std::endl;
  std::vector<TC> tripletList;
  tripletList.reserve(pow(nao*nao*K_L,2)*K_L);
  int i = 0;
  int j = 0;
  r = 0;
  s = 0;
  int rdim = nao*K_L;
  for (int k = 0; k < K_L; k++){//loop over KMO2Ints_2 blocks
    for(int x = 0; x < KMO2Ints_2[k].rows(); x++){
      for(int y = 0; y < KMO2Ints_2[k].cols(); y++){

        //Get i,j,k,l from tgems[k]
        i = tgems[k][x].kx*nao + tgems[k][x].x;
        j = tgems[k][x].ky*nao + tgems[k][x].y;
        r = tgems[k][y].kx*nao + tgems[k][y].x;
        s = tgems[k][y].ky*nao + tgems[k][y].y;
        //if (x == 0){
        //std::cout << "<" << i << "," << j << "|" << r << "," << s << ">\n";
        //}
        //VV2(i*rdim + j , r*rdim + s) = KMO2Ints_2[k](x,y);
        //VV2_AO(i*rdim + j , r*rdim + s) = KAO2ints[k](x,y);
 
        tripletList.push_back(TC(i*rdim + j, r*rdim +s, KMO2Ints_2[k](x,y)));
      }
    }
      //std::cout << "\n\n";
  }
  VV2_sparse.setFromTriplets(tripletList.begin(), tripletList.end());
  std::vector<TC>().swap(tripletList); //free memory for tripletList so we can use it again
  //Now we are in canonical ordering!  we can do symmetry and spin-adapting
  //with kronecker products of one-particle spin/symmetry adapting operators.

  //Eigen::MatrixXcd CCoeffsB = Kronecker(CoeffsB, CoeffsB);
  //Eigen::MatrixXcd VV2_MO_B = CCoeffsB.adjoint()*(VV2_AO*CCoeffsB);
  ////Now we have VV2_MO_B in canonical order.  Let's compare to VV2
  //std::cout << "(" << VV2.rows() << "," << VV2.cols() << ")\t(" << VV2_MO_B.rows() << "," << VV2_MO_B.cols() << ")\n";
  //std::complex<double> tmpvv(0,0);
  //bool identical = true;
  //for(int x = 0; x < VV2.rows(); x++){
  //    for(int y = 0; y <  VV2.cols(); y++){
  //      tmpvv = VV2_MO_B(x,y) - VV2(x,y);
  //      if (tmpvv.real() > double(1.0E-10) || tmpvv.imag() > double(1.0E-10) ){
  //          std::cout << "(" << x << "," << y << ")\t" << tmpvv << std::endl;
  //          identical = false;
  //      }
  //    }
  //}
  //if ( !identical ) { std::cout << "two-electron integrals are incorrect" << std::endl; }

  /***************************************************************************
   *
   * Now that we have 2-electron integral matrix in sparse format in the
   * canonical basis we can now perform either the complex to
   * real transformation or the purification complex->real->complex
   * 
   * ************************************************************************/


  ////Do transformation to real (C_{k} + C_{-k} ) 1/sqrt(2) = phi
  ////(C_{k} - C_{-k} ) -i/sqrt(2)
  //std::cout << "Building rotation to real matrices" << std::endl;
  //Eigen::MatrixXcd U = Eigen::MatrixXcd::Zero(K_L*nao, K_L*nao);
  //CSpMat U_sparse(nao*K_L, nao*K_L);
  ////check to see size of triplet list
  ////std::cout << tripletList.size() << std::endl;

  ////correctly populate U matrix First with plus bas
  //cnt = 0;
  //int m = 0;
  //int n = 0;
  ////iterate over Uplus bas
  //for(int k = 0; k <= K_L/2; k++){
  //  for(int j = 0; j < nao; j++){

  //    if (k == 0 || k == K_L/2){
  //      U(k*nao + j, cnt) = 1.0;
  //      tripletList.push_back(TC(k*nao + j,cnt, 1.0));
  //      cnt++;
  //    } else {
  //      m = k*nao + j;
  //      n = ( ( (-k)%K_L + K_L )%K_L   )*nao + j;
  //      std::cout << k << "\t" << ( (-k)%K_L + K_L )%K_L << std::endl;
  //      U(m,cnt) = 1./sqrt(2);
  //      U(n,cnt) = 1./sqrt(2);
  //      tripletList.push_back(TC(m,cnt, 1./sqrt(2)));
  //      tripletList.push_back(TC(n,cnt, 1./sqrt(2)));
 
  //      cnt++;
  //    }
  //  }
  //}
  ////iterate over Uminus bas
  //for(int k = 1; k < K_L/2; k++){
  //  for(int j = 0; j < nao; j++){

  //    m = k*nao + j;
  //    n = ( ( (-k)%K_L + K_L )%K_L   )*nao + j;
  //    std::cout << k << "\t" << ( (-k)%K_L + K_L )%K_L << std::endl;
  //   
  //    U(m,cnt) = std::complex<double>(0, -1./sqrt(2));
  //    U(n,cnt) = std::complex<double>(0, 1./sqrt(2));
  //    tripletList.push_back(TC(m,cnt, std::complex<double> (0,-1./sqrt(2)) ) );
  //    tripletList.push_back(TC(n,cnt, std::complex<double> (0, 1./sqrt(2)) ) );
 
  //    cnt++; 
  //  }
  //}
  //U_sparse.setFromTriplets(tripletList.begin(), tripletList.end());
  //CSpMat UU_sparse = Kronecker(U_sparse, U_sparse);
  //CSpMat UU_sparse_adjoint = UU_sparse.adjoint();
  //std::vector<TC>().swap(tripletList); //free memory for tripletList so we can use it again

  ////Now we need to populate the inverse action of this operation
  //CSpMat Uinv_sparse(nao*K_L, nao*K_L);
  //Eigen::MatrixXcd Uinv = Eigen::MatrixXcd::Zero(K_L*nao, K_L*nao);
  //cnt = 0;
  ////int key = 0;
  //std::tuple<int,int> value;
  //int kk = 0;
  //int nn = 0;
  //for(it_int_tup iterator = onebas.begin(); iterator != onebas.end(); iterator++){
  //  //key = iterator->first;
  //  value = iterator->second;
  //  kk = std::get<0>(value);
  //  nn = std::get<1>(value);

  //  //find matching ^{+}U_{ki} and ^{-}U_{ki} states
  //  if (kk == 0 || kk == K_L/2){
  //    //now find ^{+}U_{ki}
  //    //std::cout << "tuple = (" << kk << "," << nn <<  ")\n";
  //    r = Uplusbas_rev[ std::make_tuple( fabs(kk), nn) ];
  //    Uinv(r,cnt) = std::complex<double>(1.0, 0.0);
  //    tripletList.push_back(TC(r,cnt, std::complex<double> (1.0, 0.0)) );
  //    cnt++;
  //  }  else {
  //    //now find ^{+}U_{ki}, ^{-}U_{ki}
  //    r = Uplusbas_rev[ std::make_tuple( fabs(kk), nn) ];
  //    s = Uminusbas_rev[ std::make_tuple( fabs(kk), nn) ];
  //    if (kk < 0) {
  //      Uinv(r,cnt) = std::complex<double>(1./sqrt(2),0);
  //      Uinv(s + ((K_L + 2)/2)*nao ,cnt) = std::complex<double>(0,-1./sqrt(2));
  //      tripletList.push_back(TC(r,cnt, std::complex<double> (1.0/sqrt(2), 0.0)) );
  //      tripletList.push_back(TC(s + ((K_L+2)/2)*nao,cnt, std::complex<double> (0.0, -1./sqrt(2))) );
  //      cnt++;
  //    } else {
  //      Uinv(r,cnt) = std::complex<double>(1./sqrt(2),0);
  //      Uinv(s + ((K_L + 2)/2)*nao ,cnt) = std::complex<double>(0,1./sqrt(2));
  //      tripletList.push_back(TC(r,cnt, std::complex<double> (1.0/sqrt(2), 0.0)) );
  //      tripletList.push_back(TC(s + ((K_L+2)/2)*nao ,cnt, std::complex<double> (0.0, 1./sqrt(2))) );
  //      cnt++;
  //    }
  //  }
  //}

  //Uinv_sparse.setFromTriplets(tripletList.begin(), tripletList.end());
  //CSpMat UUinv_sparse = Kronecker(Uinv_sparse, Uinv_sparse);
  //CSpMat UUinv_sparse_adjoint = UUinv_sparse.adjoint();
  //std::vector<TC>().swap(tripletList); //free memory for tripletList so we can use it again

 
  ///***************************************************************************
  // *
  // * Now that we have all the relevant matrices let's do the purification.  
  // *
  // * *************************************************************************/


  ///**************************************************************************
  // *
  // * Let's do some checks:
  // *    do we have a positive-semidefinite 2-electron integral matrix.
  // *    Also we want to check if doing a forward to back transformation
  // *    preserves these eigenvalues
  // *
  // **************************************************************************/

  //Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> diag(VV2_MO_B);
  //Eigen::VectorXd VV2_eigs_complex = diag.eigenvalues();

  //CSpMat KMO2Ints_real_sparse;
  //CSpMat KMO2Ints_complex_sparse;

  //Matrix KMOHone_real; 
  //Matrix KMOMat_real; 
  //Eigen::MatrixXcd KMOHone_complex;
  //Eigen::MatrixXcd KMO2Ints_real_dense;
  //Eigen::MatrixXcd KMO2Ints_complex_dense;
  //Eigen::MatrixXcd VV2_MO_B_real_dense;
  //Eigen::MatrixXcd VV2_MO_B_complex_dense;
 

  //Eigen::MatrixXcd UU;
  //if ( K_L == 1 ){
  //  std::cout << "K_L == 1....already real. no rotation needed \n\n";
  //  KMO2Ints_real_sparse = VV2_sparse;
  //  ZeroImag(KMO2Ints_real_sparse); //helper function to zero imaginary piece of complex matrix
  //  KMOHone_real = KMOHone.real();
  //  KMOMat_real = KMOMat.real();
  //} else { 
  //  UU = Kronecker(U, U);
  //  KMO2Ints_real_sparse = VV2_sparse;
  //  KMO2Ints_real_dense = VV2;
  //  KMO2Ints_real_sparse = (UU_sparse_adjoint*KMO2Ints_real_sparse)*UU_sparse;
  //  KMO2Ints_real_dense = ((UU.adjoint()*KMO2Ints_real_dense)*UU);
  //  VV2_MO_B_real_dense = (UU.adjoint()*VV2_MO_B)*UU;
  //  ZeroImag(KMO2Ints_real_sparse); //helper function to zero imaginary piece of complex matrix
  //  ZeroImag(KMO2Ints_real_dense); //helper function to zero imaginary piece of complex matrix
  //  ZeroImag(VV2_MO_B_real_dense); //helper function to zero imaginary piece of complex matrix
  //  KMOHone_real = ((U.adjoint()*KMOHone)*U).real();
  //  KMOMat_real = ((U.adjoint()*KMOMat)*U).real();
  //}

  ////std::complex<double> realdiff;
  ////for(int x = 0; x < VV2_MO_B_real_dense.rows(); x++){
  ////    for(int y = 0; y < VV2_MO_B_real_dense.cols(); y++){
  ////        realdiff = VV2_MO_B_real_dense(x,y) - KMO2Ints_real_dense(x,y);
  ////        if (realdiff.real() > double(1.0E-10) || realdiff.imag() > double(1.0E-10)){
  ////            std::cout << realdiff << std::endl;
  ////        }
  ////    }
  ////}

  ////Now check eigenvalues of KMO2Int_real_dense
  //Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> diagr(KMO2Ints_real_dense);
  //Eigen::VectorXd VV2_eigs_real = diagr.eigenvalues();
  //Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> diagrB(VV2_MO_B_real_dense);
  //Eigen::VectorXd VV2_MO_B_eigs = diagrB.eigenvalues();
 
  ////for(int i = 0; i < VV2_eigs_real.rows(); i++){
  ////  std::cout<<VV2_eigs_real(i) - VV2_eigs_complex(i) << std::endl;
  ////}  
  //
  //////Now convert back
  //Eigen::MatrixXcd UUinv = Kronecker(Uinv, Uinv);
  //KMOHone_complex = (Uinv.adjoint()*KMOHone_real)*Uinv;
  //KMO2Ints_complex_sparse = UUinv_sparse_adjoint*(KMO2Ints_real_sparse*UUinv_sparse);
  //KMO2Ints_complex_dense = UUinv.adjoint()*(KMO2Ints_real_dense*UUinv);
  //VV2_MO_B_complex_dense = UUinv.adjoint()*(VV2_MO_B_real_dense*UUinv);
 
  //Eigen::MatrixXcd BackToReal = UU.adjoint()*(VV2_MO_B_complex_dense*UU);
  //Eigen::MatrixXcd BackToComplex = UUinv.adjoint()*(BackToReal*UUinv);
 
  //std::complex<double> diff;
  //for(int x = 0; x < BackToReal.rows(); x++){
  //    for(int y = 0; y < BackToReal.cols(); y++){
  //        diff =  BackToReal(x,y) - VV2_MO_B_real_dense(x,y);
  //        if (diff.real() > double(1.0E-10) || diff.imag() > double(1.0E-10) ){
  //            std::cout << diff << std::endl;
  //        }
  //    }
  //}
  //for(int x = 0; x < BackToComplex.rows(); x++){
  //    for(int y = 0; y < BackToComplex.cols(); y++){
  //        diff =  BackToComplex(x,y) - VV2_MO_B_complex_dense(x,y);
  //        if (diff.real() > double(1.0E-10) || diff.imag() > double(1.0E-10) ){
  //            std::cout << diff << std::endl;
  //        }
  //    }
  //}





  //Eigen::SelfAdjointEigenSolver<Eigen::MatrixXcd> diagc(KMO2Ints_complex_dense);
  //Eigen::VectorXd VV2_eigs_complex_2 = diagr.eigenvalues();
  ////for(int i = 0; i < VV2_eigs_complex_2.rows(); i++){
  ////  std::cout<< VV2_eigs_real(i) - VV2_eigs_complex_2(i) << std::endl;
  ////}  
  ////std::cout << KMO2Ints_complex_dense.trace() - VV2_MO_B.trace() << std::endl;
  //


 
  ////for(int k = 0; k < KMO2Ints_complex_sparse.outerSize(); ++k){
  ////  for(CSpMat::InnerIterator it(KMO2Ints_complex_sparse,k); it; ++it){
  ////      std::cout << it.value() -  VV2_sparse.coeffRef(it.row(), it.col() ) << std::endl; //value of element.  expensive binary search for reference value in copy.
  ////      //it.row(); //row index of element
  ////      //it.col(); //col index of element
  ////      //it.index(); //index equal to col index
  ////  }
  ////}
  ////std::cout << "Dense sizes" << std::endl;
  ////std::cout << KMO2Ints_complex_dense.rows() << "\t" << VV2_dense.rows() << std::endl;
  ////for(int x = 0; x < VV2_dense.rows(); x++){
  ////  for(int y = 0; y < VV2_dense.cols(); y++){
  ////     tmpc =  VV2_dense(x,y) - KMO2Ints_complex_dense(x,y) ;
  ////     if (tmpc.real() > double(1.0E-10) || tmpc.imag() > double(1.0E-10)) {
  ////         std::cout << tmpc << std::endl;
  ////     }
  ////  }
  ////}

  std::cout << "Larges mat_mul completed" << std::endl;
  Eigen::MatrixXcd KGmat_real = Eigen::MatrixXcd::Zero(nao*K_L, nao*K_L);

  std::cout << "Finished Building real matrices" << std::endl;
  std::cout << "start building KGmat_real" << std::endl;

  std::cout << "looping over non-zeros of KMOMat_real_sparse" << std::endl;
  i = 0;
  j = 0;
  r = 0;
  s = 0;
  //for(int i = 0; i < nao*K_L; i++){
  //    for(int j = 0; j < nao*K_L; j++){
  //        for(int r = 0; r < nao*K_L; r++){
  //            for(int s = 0; s < nao*K_L; s++){

  //                KGmat_real(i,r) += KMOMat(s,j)*(VV2_sparse.coeffRef(i*nao*K_L + j, r*nao*K_L + s) - 0.5*VV2_sparse.coeffRef(i*nao*K_L + j, s*nao*K_L + r) );
  //            }
  //        }
  //    }
  //}
  ////Coulomb Piece
  for(int a = 0; a < VV2_sparse.outerSize(); ++a){
      for(CSpMat::InnerIterator it(VV2_sparse, a); it; ++it){

        i = it.row()/(nao*K_L);
        j = ( (it.row() % (nao*K_L)) + (nao*K_L) ) % (nao*K_L);
        r = it.col()/(nao*K_L);
        s = ( (it.col() % (nao*K_L)) + (nao*K_L) ) % (nao*K_L);
        KGmat_real(i, r) += KMOMat(s,j)*it.value();

      }
  }
  //Exchange Piece
  for(int a = 0; a < VV2_sparse.outerSize(); ++a){
      for(CSpMat::InnerIterator it(VV2_sparse, a); it; ++it){
        i = it.row()/(nao*K_L);
        j = ( (it.row() % (nao*K_L)) + (nao*K_L) ) % (nao*K_L);
        r = it.col()/(nao*K_L);
        s = ( (it.col() % (nao*K_L)) + (nao*K_L) ) % (nao*K_L);
        KGmat_real(i, s) -= 0.5*KMOMat(r,j)*it.value();

      }
  }

  std::cout << "Finish building KGmat_real " << std::endl;

  std::cout << (KMOMat*KGmat_real).trace().real() << std::endl;
  std::cout << "One Electron Energy\n";
  std::cout << ((KMOMat*KMOHone).trace()/double(K_L)).real() << std::endl;
  std::cout << "Two Electron Energy\n";
  std::cout <<  (((0.5*KMOMat)*KGmat_real).trace()/double(K_L)).real() << std::endl;
 
  std::cout << "Total Energy ";
  std::cout << ((KMOMat*KMOHone).trace()/double(K_L) + 
      ((0.5*KMOMat)*KGmat_real).trace()/double(K_L) + enuc).real() + EMPE << std::endl;

  //Matrix D2HF = Kronecker(KMOMat_real, KMOMat_real);
  //
  std::ofstream ofs;
  ofs.precision(15);
  ofs.open("Integrals.dat", std::ofstream::out);
  for(int i = 0; i < K_L*nao; i++){
    for(int j = i; j < K_L*nao; j++){
      ofs << i+1 << "\t" << j+1 << "\t0\t0\t" << KMOHone(i,j).real() << "\t" << KMOHone(i,j).imag() << "\n";
      //ofs << i+1 << "\t" << j+1 << "\t0\t0\t" << KMOHone_real(i,j) <<  "\n";
    }
  }
  cnt= 0;
  //for(int i = 0; i < rdim; i++){
  //    for(int j = 0; j < rdim; j++){
  //        for(int k = 0; k < rdim; k++){
  //            for(int l = 0; l < rdim; l++){
  //                //if (VV2_MO_B_complex_dense(i*rdim + j, k*rdim + l).real() > double(1.0E-10) ||
  //                //    VV2_MO_B_complex_dense(i*rdim + j, k*rdim + l).imag() > double(1.0E-10)  ){
  //                if (std::norm(VV2_sparse.coeffRef(i*rdim + j, k*rdim + l)) > double(1.0E-10) && k*rdim + l >= i*rdim + j ){
 
  //                //if (VV2_MO_B_real_dense(i*rdim + j, k*rdim + l).real() > double(1.0E-10) ){
  //              ofs << i + 1 << "\t"
  //                << j + 1 << "\t"
  //                << k + 1 << "\t"
  //                << l + 1 << "\t"
  //                //<< VV2_MO_B_complex_dense(i*rdim + j, k*rdim + l).real() << "\t"
  //                //<< VV2_MO_B_complex_dense(i*rdim + j, k*rdim + l).imag() << std::endl;
  //                << VV2_sparse.coeffRef(i*rdim + j, k*rdim + l).real() << "\t"
  //                << VV2_sparse.coeffRef(i*rdim + j, k*rdim + l).imag() << std::endl;
  //                //<< VV2_MO_B_real_dense(i*rdim + j, k*rdim + l).real() << "\n";
  //              cnt++;
  //                }
  //            }
  //        }
  //    }
  //}

  for(int a = 0; a < VV2_sparse.outerSize(); ++a){
      for(CSpMat::InnerIterator it(VV2_sparse, a); it; ++it){

        i = it.row()/(nao*K_L);
        j = ( (it.row() % (nao*K_L)) + (nao*K_L) ) % (nao*K_L);
        r = it.col()/(nao*K_L);
        s = ( (it.col() % (nao*K_L)) + (nao*K_L) ) % (nao*K_L);
        if (std::norm(it.value()) > double(1.0E-10) && r*rdim + s >= i*rdim + j )  {
        ofs << i + 1 << "\t"
          << j + 1 << "\t"
          << r + 1 << "\t"
          << s + 1 << "\t"
          << it.value().real() << "\t" << it.value().imag() << "\n";
        cnt++;
        }
      }
  }
  ofs.close();


  std::cout << "Num Nonzeros in 2-ints "  << cnt << std::endl; 
  //ofs.open ("D2HF.rdm", std::ofstream::out);
  //for(int i = 0; i < D2HF.rows(); i++){
  //    for(int j = 0; j < D2HF.cols(); j++){
  //        ofs << D2HF(i,j) << "\n";
  //    }
  //}



}

void rotateAO2KAO2intsOne( Eigen::MatrixXcd& KAO2ints, std::vector<double>& kvec, std::vector<gem>& basis,  int nao, double a, int K_L, double kval, BasisSet& obs, std::vector<Atom>& atoms, std::vector<double>& TV ){
 

    bas_map gem_bas; 
    bas_map_rev gem_bas_rev;
    int p = 0;
    int q = 0;
    //int kp = 0;
    int kq = 0;
    int r = 0;
    int s = 0;
    int kr = 0;
    int ks = 0;
    std::complex<double> ii(0,1);
    Matrix tmp_integrals_c;
    const auto tstart = std::chrono::high_resolution_clock::now();
    for (int m1 = -NAMUR_S; m1 < NAMUR_S+1; m1++){
      for (int m2 = -NAMUR_L; m2 < NAMUR_L+1; m2++){
        for(int m3 = -NAMUR_S; m3 < NAMUR_S + 1; m3++){

            tmp_integrals_c = compute_2body_ints(obs, m1, m2, m3+m2, nao, TV, atoms);
              //for a given m1, m2, m3 index loop over the matrix and update
              //the k-space a.o. matrix with the appropriately scaled valued.
              for(int x = 0; x < KAO2ints.rows(); x++){
                for(int y = 0; y < KAO2ints.cols();  y++){ 

                  //row info
                  p = basis[x].x;
                  q = basis[x].y;
                  //kp = basis[x].kx;
                  kq = basis[x].ky;
                  //col info
                  r = basis[y].x;
                  s = basis[y].y;
                  kr = basis[y].kx;
                  ks = basis[y].ky;
          
                  double exponent = (m1*kvec[kr]-m2*kvec[kq]+(m3+m2)*kvec[ks])*a;
                  KAO2ints(x,y) += std::complex<double>(tmp_integrals_c(p*nao+q, r*nao+s)*cos(exponent ), tmp_integrals_c(p*nao+q, r*nao+s)*sin( exponent ) );
                }
              }

        }//m3
      }//m2
    }//m1

    KAO2ints *= (1./K_L);
     
    ////loop over and symmeterize <ij|kl> = <ji|lk> = <kl|ij>* = <lk|ji>*
    //int new_x = 0;
    //int new_y = 0;
    //for(std::map<int, geminal>::iterator xx=gem_bas.begin(); xx!=gem_bas.end(); xx++){
    //    for(std::map<int, geminal>::iterator yy=gem_bas.begin(); yy!=gem_bas.end(); yy++){
    //        //(*xx).first = key
    //        //(*xx).second ;//(ii)(jj) 
    //        new_x = gem_bas_rev[ geminal( std::get<1>((*xx).second), std::get<0>((*xx).second) ) ] ; // x-new
    //        //(*yy).second ;//(kk)(ll)
    //        new_y = gem_bas_rev[ geminal( std::get<1>((*yy).second), std::get<0>((*yy).second) ) ] ; // y-new
    //        KAO2ints( (*xx).first , (*yy).first ) = 0.25*(KAO2ints( (*xx).first, (*yy).first ) +  KAO2ints( new_x, new_y) + std::conj(KAO2ints( (*yy).first , (*xx).first )) + std::conj(KAO2ints( new_y, new_x ) ) );
    //        KAO2ints( new_x, new_y ) =  KAO2ints( (*xx).first , (*yy).first );
    //        KAO2ints( (*yy).first , (*xx).first ) = std::conj(KAO2ints((*xx).first , (*yy).first ));
    //        KAO2ints( new_y, new_x ) =  std::conj(KAO2ints( (*xx).first , (*yy).first ));

    //    }
    //}


     const auto tstop = std::chrono::high_resolution_clock::now();
      const std::chrono::duration<double> time_elapsed = tstop - tstart;
      std::cout << "Time elapsed " << time_elapsed.count() << "\n";


}


