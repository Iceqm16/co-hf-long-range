#include "integrals.hpp"
#include "globalvars.hpp"
#include "mathutilities.hpp"

void ReadGuessDensity(std::string filename, int nao, std::vector<Eigen::MatrixXcd>& xD){

  std::cout << filename << std::endl;
  std::cout << nao << std::endl;
  std::cout << K_L << std::endl;
  std::ifstream myfile (filename);
  std::string line;
  std::string::size_type sz;  
  if (myfile.is_open())
  {
        for(int i= 0; i < xD.size(); i++){
            for(int x = 0; x < nao; x++){
                for(int y = 0; y < nao; y++){
                    
                    if (std::getline(myfile, line)){
                        xD[i](x,y) = std::stod(line, &sz);
                        //std::cout << line << '\n';
                    }
                    else{
                        myfile.close();
                        return;
                    }
    
                }
            }
        }
    myfile.close();
  }
}
