#ifndef GLOBALVARS_HPP
#define GLOBALVARS_HPP

/* declaration of variables that will be global */
/* definition occurs in integrals */
extern int NAMUR_L;
extern int NAMUR_S;
extern std::string cutoff_type;
extern int K_L;
extern std::string basis_set;
extern std::string guess_density;
extern int PRINT; 
extern double LR;
#endif
