/*
 * Function definitions for Constructing G-matrix for Hartree-Fock based on
 * Namur cut off criteria.
 *
 */

#include "integrals.hpp"
#include "globalvars.hpp"
#include "mathutilities.hpp"

void ConstructG_Namur(std::vector<Eigen::MatrixXcd>& xD,  std::vector<int>& translate, 
        std::vector<Eigen::MatrixXcd>& Gmat,
        std::vector<Eigen::MatrixXcd>& J2, std::vector<Eigen::MatrixXcd>& X2, 
        int nao, int K_L, std::vector<double>& TV, 
        BasisSet& obs, std::vector<Atom>& atoms ){

  Eigen::MatrixXcd tmp = Eigen::MatrixXcd::Zero(nao,nao);
  Eigen::MatrixXcd tmp2 = Eigen::MatrixXcd::Zero(nao,nao);
  Eigen::MatrixXcd tmp3 = Eigen::MatrixXcd::Zero(nao,nao);
  Matrix tmp_integrals_c;
  Matrix tmp_integrals_x;



  for(int m1 = -NAMUR_S; m1 < NAMUR_S+1; m1++){

    for(int i = 0; i < nao; i++){
      for(int j = 0; j < nao; j++){

        for(int m2 = -NAMUR_L; m2 < NAMUR_L+1; m2++){
          for(int m3 = -NAMUR_S; m3 < NAMUR_S + 1; m3++){

            //for each m1, m2, m3 compute the two-electron integral matrix.
            //then perform summation.  Never storing this.  We can use the
            //2-body integral generator we wrote before without too much
            //modification.  after we use each integral matrix we can discard
            //it saving memory.  Let's also make sure garbage collection works
            //so we aren't accumulating temporary 2-electron integral matrices.  
            tmp_integrals_c = compute_2body_ints(obs, m1, m2, m3+m2, nao, TV, atoms);
            tmp_integrals_x = compute_2body_ints(obs, m3+m2, m2, m1, nao, TV, atoms);

              for(int s = 0; s < nao; s++){
                for(int r = 0; r < nao; r++){

            tmp(i,s) +=  xD[m3+NAMUR_S](r,j)*(tmp_integrals_c(i*nao + j, s*nao+r) -
                    0.5*tmp_integrals_x(i*nao+j, r*nao+s) );
 
            tmp2(i,s) += xD[m3+NAMUR_S](r,j)*tmp_integrals_c(i*nao + j, s*nao+r);
            tmp3(i,s) -= 0.5*xD[m3+NAMUR_S](r,j)*tmp_integrals_x(i*nao+j, r*nao+s);
 
 
                }//r
              }//s
            
            }//m3
          }//m2

        }//j
      }//i

    Gmat[m1+NAMUR_S] += tmp;
    J2[m1+NAMUR_S] += tmp2;
    X2[m1+NAMUR_S] += tmp3;
    ZeroMatrix(tmp);
    ZeroMatrix(tmp2);
    ZeroMatrix(tmp3);
    
  }

}

void ConstructG_Namur_2(std::vector<Eigen::MatrixXcd>& xD,  std::vector<int>& translate, 
        std::vector<Eigen::MatrixXcd>& Gmat,
        std::vector<Eigen::MatrixXcd>& J2, std::vector<Eigen::MatrixXcd>& X2, 
        int nao, int K_L, std::vector<double>& TV, 
        BasisSet& obs, std::vector<Atom>& atoms ){

  Matrix tmp_integrals_c;
  Matrix tmp_integrals_x;

  int i, j, s, r, m2,m3,m1;
  for( m1 = 0; m1 < NAMUR_S+1; ++m1){

    for( m2 = -NAMUR_L; m2 < NAMUR_L+1; ++m2){
      for( m3 = -NAMUR_S; m3 < NAMUR_S + 1; ++m3){

        tmp_integrals_c = compute_2body_ints(obs, m1, m2, m3+m2, nao, TV, atoms);
        tmp_integrals_x = compute_2body_ints(obs, m3+m2, m2, m1, nao, TV, atoms);

        for(i = 0; i < nao; ++i){//alpha*
          for(j = 0; j < nao; ++j){//beta*

              for(s = 0; s < nao; ++s){//alpha
                for(r = 0; r < nao; ++r){//beta
            
                    Gmat[m1+NAMUR_S](i,s) += xD[m3+NAMUR_S](r,j)*(tmp_integrals_c(i*nao + j, s*nao+r) -
                    0.5*tmp_integrals_x(i*nao+j, r*nao+s) );


                    J2[m1+NAMUR_S](i,s) += xD[m3+NAMUR_S](r,j)*tmp_integrals_c(i*nao + j, s*nao+r);
                    X2[m1+NAMUR_S](i,s) -= 0.5*xD[m3+NAMUR_S](r,j)*tmp_integrals_x(i*nao+j, r*nao+s);
 
              }//r
            }//s
          }//j
        }//i

           
      }//m3
    }//m2
    
    if (m1 != 0) {
    Gmat[-m1+NAMUR_S] = Gmat[m1+NAMUR_S].transpose();
    }
  }//m1
    
}



void ConstructG2(std::vector<Eigen::MatrixXcd>& xD, std::vector<Matrix>& TwoInts, std::vector<int>& translate, std::vector<Eigen::MatrixXcd>& Gmat,  int nao, int K_L){

 
  //Generate dictionary equivalent. Use some datastructure
  int L = 2*NAMUR_L + 1;


  for(const auto &  m1: translate){
        
    Eigen::MatrixXcd tmp = Eigen::MatrixXcd::Zero(nao,nao);
    for(auto j = 0; j < nao; j++){
      for(auto i = 0; i < nao; i++){
                
        for(const auto & m2: translate){
          for(const auto & m3: translate){

            int idx_c2el = (m1+NAMUR_L)*pow(double(L), 2.) + (m2+NAMUR_L)*L + (m3+NAMUR_L);
            int idx_x2el = (m3+NAMUR_L)*pow(double(L), 2.) + (m2+NAMUR_L)*L + (m1+NAMUR_L);

              for(auto s = 0; s < nao; s++){
                for(auto r = 0; r < nao; r++){
                  tmp(j,i) += xD[m3-m2+2*NAMUR_L](r,s)*TwoInts[idx_c2el](j*nao + s, i*nao+r);
                  tmp(j,i) -= xD[m3-m2+2*NAMUR_L](r,s)*0.5*TwoInts[idx_x2el](j*nao+s, r*nao+i);
                }
              }
            
            }
          }
        }
      }

    Gmat[m1+NAMUR_L] += tmp;
  }
}





void ConstructG(std::vector<Eigen::MatrixXcd>& xD, std::vector<Matrix>& TwoInts, std::vector<int>& translate, std::vector<Eigen::MatrixXcd>& Gmat,  int nao, int K_L){

 
  //Generate dictionary equivalent. Use some datastructure
  int L = 2*NAMUR_L + 1;


  for(const auto &  m1: translate){
        
    Eigen::MatrixXcd tmp = Eigen::MatrixXcd::Zero(nao,nao);
    for(auto j = 0; j < nao; j++){
      for(auto i = 0; i < nao; i++){
                
        for(const auto & m2: translate){
          for(const auto & m3: translate){

            int idx_c2el = (m1+NAMUR_L)*pow(double(L), 2.0) + (m2+NAMUR_L)*L + (m3+NAMUR_L);
            int idx_x2el = (m3+NAMUR_L)*pow(double(L), 2.0) + (m2+NAMUR_L)*L + (m1+NAMUR_L);

            if (abs(m3-m2) <= NAMUR_L){
              for(auto s = 0; s < nao; s++){
                for(auto r = 0; r < nao; r++){
                  tmp(j,i) += xD[m3-m2+NAMUR_L](r,s)*TwoInts[idx_c2el](j*nao + s, i*nao+r);
                  tmp(j,i) -= xD[m3-m2+NAMUR_L](r,s)*0.5*TwoInts[idx_x2el](j*nao+s, r*nao+i);
                }
              }
            } 


          }
        }
      }
    }

    Gmat[m1+NAMUR_L] += tmp;
  }
}

void ConstructG_cellwise(std::vector<Eigen::MatrixXcd>& xD, std::vector<Matrix>& TwoInts, std::vector<int>& translate, std::vector<Eigen::MatrixXcd>& Gmat,  int nao, int K_L){

 
  //Generate dictionary equivalent. Use some datastructure
  int L = 2*NAMUR_L + 1;


  for(const auto &  m1: translate){
        
    Eigen::MatrixXcd tmp = Eigen::MatrixXcd::Zero(nao,nao);

    auto t_m2 = (m1 >= 0) ? -1*NAMUR_L+m1 : -1*NAMUR_L;
    auto t_m2_end = (m1 >= 0) ? NAMUR_L : NAMUR_L + m1;

    auto t_m3 = (m1 >= 0) ? -1*NAMUR_L+m1 : -1*NAMUR_L;
    auto t_m3_end = (m1 >= 0) ? NAMUR_L : NAMUR_L + m1;


    //printf("m1 = %i, m2_start = %i, m2_end = %i, m3_start = %i, m3_end = %i \n", m1, t_m2, t_m2_end, t_m3, t_m3_end);
    for(auto j = 0; j < nao; j++){
      for(auto i = 0; i < nao; i++){
        
        for(auto m2 = t_m2; m2 < t_m2_end; m2++){
          for(auto m3 = t_m3; m3 < t_m3_end; m3++){

            int idx_c2el = (m1+NAMUR_L)*pow(double(L), 2.) + (m2+NAMUR_L)*L + (m3+NAMUR_L);
            int idx_x2el = (m3+NAMUR_L)*pow(double(L), 2.) + (m2+NAMUR_L)*L + (m1+NAMUR_L);

            if (abs(m3-m2) <= NAMUR_L){
              for(auto s = 0; s < nao; s++){
                for(auto r = 0; r < nao; r++){
                  tmp(j,i) += xD[m3-m2+NAMUR_L](r,s)*TwoInts[idx_c2el](j*nao + s, i*nao+r);
                  tmp(j,i) -= xD[m3-m2+NAMUR_L](r,s)*0.5*TwoInts[idx_x2el](j*nao+s, r*nao+i);
                }
              }
            } else {
               //printf("m1 %i,m3 = %i, m2= %i, m3-m2 = %i\n", m1, m3, m2, m3-m2);
                 if ((m3 - m2) < 0) {
                     //std::cout << "test1" << "\n"; 
                     //std::cout << m3 - m2 << "\n";
                     //std::cout << m3 - m2 + 2*NAMUR_L + 1 << "\n";
                     //std::cout << m3 - m2 + 2*NAMUR_L + 1 + NAMUR_L << "\n";
                for(auto s = 0; s < nao; s++){
                 for(auto r = 0; r < nao; r++){
                   tmp(j,i) += xD[m3-m2+NAMUR_L + 2*NAMUR_L+1](r,s)*TwoInts[idx_c2el](j*nao + s, i*nao+r);
                   tmp(j,i) -= xD[m3-m2+NAMUR_L + 2*NAMUR_L+1](r,s)*0.5*TwoInts[idx_x2el](j*nao+s, r*nao+i);
                 }
               }
 

                 } else {
                     //std::cout << "test2" << "\n";
                     //std::cout << m3 - m2 << "\n";
                     //std::cout << m3 - m2 - (2*NAMUR_L + 1) << "\n";
                     //std::cout << m3 - m2 - (2*NAMUR_L + 1) + NAMUR_L << "\n";
               for(auto s = 0; s < nao; s++){
                 for(auto r = 0; r < nao; r++){
                   tmp(j,i) += xD[m3-m2 - (2*NAMUR_L+1) +NAMUR_L](r,s)*TwoInts[idx_c2el](j*nao + s, i*nao+r);
                   tmp(j,i) -= xD[m3-m2-(2*NAMUR_L+1)+NAMUR_L](r,s)*0.5*TwoInts[idx_x2el](j*nao+s, r*nao+i);
 
                 }
               }

                 }        

             }


          }
        }
      }
    }
    Gmat[m1+NAMUR_L] += tmp;
  }
}


