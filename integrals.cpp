#if __cplusplus <= 199711L
# error "Program requires C++11 support"
#endif

// integrals include
#include "integrals.hpp"
#include "globalvars.hpp"
#include "mathutilities.hpp"


/* Definition of variables */
int NAMUR_L = 5;
int NAMUR_S = 5;
std::string cutoff_type = "Nocut";
int K_L = 100;
std::string basis_set = "sto-3g";
std::string guess_density = "CORE";
int PRINT = 1;
double LR = 0.0;
 

int main(int argc, char *argv[]) {

  using std::cout;
  using std::cerr;
  using std::endl;

 
  Eigen::IOFormat HeavyFmt(15, 0, ", ", ",\n", "[", "]", "[", "]");

  try {


    std::vector<double> TV = {0.0 , 0.0 , 0.0};
    cout.precision(15);

    /*** =========================== ***/
    /*** initialize molecule         ***/
    /*** =========================== ***/

    const auto filename = (argc > 1) ? argv[1] : "nofile.xyz";
    std::vector<Atom> atoms = read_geometry(filename, TV, cutoff_type);


    /*** =========================== ***/
    /*** create basis set            ***/
    /*** Each Box will have the same ***/
    /*** Basis functions as box zero ***/
    /*** =========================== ***/

    BasisSet obs(basis_set, atoms);
    for(int i = 0; i < obs.size(); i++){
        std::cout << "Orbital "<< i << std::endl;
        std::cout << obs[i] << std::endl;
    }
 
    cout << "\tbasis rank = " << obs.nbf() << endl;
    printf("\n\tCut off type = %s\n", cutoff_type.c_str());
    printf("\tK_L = %i\n", K_L);
    
    size_t nao = 0;
    for (auto s=0; s<obs.size(); ++s)
      nao += obs[s].size();

    int nocc = 0;
    for(auto i = 0; i < atoms.size(); i++){
        nocc += atoms[i].atomic_number;
    }
    nocc = 0.5*nocc;


    /*** =========================== ***/
    /*** compute 1-e integrals       ***/
    /*** =========================== ***/

    libint2::init();

    std::vector<int> translateS(2*NAMUR_S + 1); 
    std::iota(std::begin(translateS), std::end(translateS), -1*NAMUR_S);

    std::vector<int> translateL(2*NAMUR_L + 1); 
    std::iota(std::begin(translateL), std::end(translateL), -1*NAMUR_L);
    
   
    auto enuc = 0.0;
    
    printf("\n\tATOMS\n");
    printf("\tX\tY\tZ\tZ\n");
    for(auto i = 0; i < atoms.size(); i++){
        printf("\t%f\t%f\t%f\t%d\n", atoms[i].x, atoms[i].y, atoms[i].z, 
                atoms[i].atomic_number);
    }

    //calculate the nuclear repulsion over translation of L
    for (auto i = 0; i < atoms.size(); i++){
      for (auto j = 0; j < atoms.size(); j++) {

          for(auto l = 0; l < translateL.size(); l++){
              if( i == j && translateL[l] == 0){
                    //Divergent terms
                  //std::cout << "divergent term " << "(" << i << "," << j 
                  //    << ")" << " " << translateL[l] << std::endl;
                    ;
              } else {
                auto xij = atoms[i].x - (TV[0]*translateL[l] + atoms[j].x);
                auto yij = atoms[i].y - (TV[1]*translateL[l] + atoms[j].y);
                auto zij = atoms[i].z - (TV[2]*translateL[l] + atoms[j].z);
                auto r2 = xij*xij + yij*yij + zij*zij;
                auto r = sqrt(r2);
                enuc += 0.5*atoms[i].atomic_number * atoms[j].atomic_number / r;
              }
          }
      }
    }
    
    cout << "\n\tNuclear repulsion energy = " << enuc << endl;

    /*********************************
     *
     *  Compute 1-el pieces 
     *
     * ******************************/

    
    std::vector<Matrix> Kinetic; Kinetic.resize(2*NAMUR_S + 1);
    AssignZeroList(Kinetic, nao);
    for(int i = 0; i < NAMUR_S + 1; ++i){
      auto T = compute_1body_ints<libint2::OneBodyEngine::kinetic>(obs,  TV, translateS[i])[0];
      Kinetic[i] = T;
      Kinetic[2*NAMUR_S - i] = T.transpose();
    }
    std::vector<Matrix> Potential; Potential.resize(2*NAMUR_S + 1);
    AssignZeroList(Potential, nao);
//#if defined(_OPENMP)
//#pragma omp parallel for
//#endif
    for(int i = 0; i < NAMUR_S+1; ++i){
      auto V = compute_1body_ints<libint2::OneBodyEngine::nuclear>(obs,  TV, translateS[i], atoms)[0];
      Potential[i] = V;
      Potential[2*NAMUR_S - i] = V.transpose();
    }
    
    std::vector<Matrix> XDipole; XDipole.resize(2*NAMUR_S + 1);
    AssignZeroList(XDipole, nao);
    std::vector<Matrix> YDipole; YDipole.resize(2*NAMUR_S + 1);
    AssignZeroList(YDipole, nao);
    std::vector<Matrix> ZDipole; ZDipole.resize(2*NAMUR_S + 1);
    AssignZeroList(XDipole, nao);
    std::vector<Matrix> XXQuad; XXQuad.resize(2*NAMUR_S + 1);
    AssignZeroList(XXQuad, nao);
    std::vector<Matrix> YYQuad; YYQuad.resize(2*NAMUR_S + 1);
    AssignZeroList(YYQuad, nao);
    std::vector<Matrix> ZZQuad; ZZQuad.resize(2*NAMUR_S + 1);
    AssignZeroList(ZZQuad, nao);
    std::vector<Matrix> XYQuad; XYQuad.resize(2*NAMUR_S + 1);
    AssignZeroList(XYQuad, nao);
    std::vector<Matrix> XZQuad; XZQuad.resize(2*NAMUR_S + 1);
    AssignZeroList(XZQuad, nao);
    std::vector<Matrix> YZQuad; YZQuad.resize(2*NAMUR_S + 1);
    AssignZeroList(YZQuad, nao);
    std::vector<Matrix> Overlaps; Overlaps.resize(2*NAMUR_S + 1);
    AssignZeroList(Overlaps, nao);

//figure out if this is save with std::vector<> 
//#if defined(_OPENMP)
//#pragma omp parallel for
//#endif
    for(int i = 0; i < 2*NAMUR_S + 1; ++i){
      auto Mu = compute_1body_ints<libint2::OneBodyEngine::emultipole2>(obs,  TV, translateS[i], atoms);
      Overlaps[i] = Mu[0];
      XDipole[i] = Mu[1];
      YDipole[i] = Mu[2];
      ZDipole[i] = Mu[3];
      XXQuad[i] = Mu[4];
      XYQuad[i] = Mu[5];
      XZQuad[i] = Mu[6];
      YYQuad[i] = Mu[7];
      YZQuad[i] = Mu[8];
      ZZQuad[i] = Mu[9];

      //Overlaps[2*NAMUR_S - i] = Mu[0].transpose();
      //XDipole[2*NAMUR_S - i] = Mu[1].transpose();
      //YDipole[2*NAMUR_S - i] = Mu[2].transpose();
      //ZDipole[2*NAMUR_S - i] = Mu[3].transpose();
      //XXQuad[2*NAMUR_S - i] = Mu[4].transpose();
      //XYQuad[2*NAMUR_S - i] = Mu[5].transpose();
      //XZQuad[2*NAMUR_S - i] = Mu[6].transpose();
      //YYQuad[2*NAMUR_S - i] = Mu[7].transpose();
      //YZQuad[2*NAMUR_S - i] = Mu[8].transpose();
      //ZZQuad[2*NAMUR_S - i] = Mu[9].transpose();
    }

  //for(int i = 0; i < 2*NAMUR_S+1; i++){
  //    std::cout << "Overlap matrix " << i << std::endl;
  //    std::cout << Overlaps[i] << std::endl;
  //}
  //std::cout << "\n\n\n";
  //for(int i = 0; i < 2*NAMUR_S+1; i++){
  //    std::cout << "XX-quadrupole matrix " << i << std::endl;
  //    std::cout << XXQuad[i] << std::endl;
  //}
  //std::cout << "\n\n\n";
  //for(int i = 0; i < 2*NAMUR_S+1; i++){
  //    std::cout << "XY-quadrupole matrix " << i << std::endl;
  //    std::cout << XYQuad[i] << std::endl;
  //}
  //std::cout << "\n\n\n";
  //for(int i = 0; i < 2*NAMUR_S+1; i++){
  //    std::cout << "XZ-quadrupole matrix " << i << std::endl;
  //    std::cout << XZQuad[i] << std::endl;
  //}
  //std::cout << "\n\n\n";
  //for(int i = 0; i < 2*NAMUR_S+1; i++){
  //    std::cout << "YY-quadrupole matrix " << i << std::endl;
  //    std::cout << YYQuad[i] << std::endl;
  //}
  //std::cout << "\n\n\n";
  //for(int i = 0; i < 2*NAMUR_S+1; i++){
  //    std::cout << "YZ-quadrupole matrix " << i << std::endl;
  //    std::cout << YZQuad[i] << std::endl;
  //}
  //std::cout << "\n\n\n";
  //for(int i = 0; i < 2*NAMUR_S+1; i++){
  //    std::cout << "ZZ-quadrupole matrix " << i << std::endl;
  //    std::cout << ZZQuad[i] << std::endl;
  //}



  std::ofstream ofsOlaps;
  ofsOlaps.precision(15);
  ofsOlaps.open ("Overlaps.dat", std::ofstream::out );
    for(auto i = 0; i < translateS.size(); i++){
        ofsOlaps << "\tcell = " << translateS[i] << "\n";
        ofsOlaps << Overlaps[i].format(HeavyFmt) << "\n\n";
 
    }
  ofsOlaps.close();

  std::vector<Matrix> tVV;
  for(auto i(0); i < 2*NAMUR_S+1; i++){
    tVV.push_back(Potential[i]);
  }

    /*** =========================== ***/
    /*** Fourier Transform of        ***/
    /*** one-electron quantities     ***/
    /*** =========================== ***/

    std::vector<Eigen::MatrixXcd> KOverlaps;
    std::vector<Eigen::MatrixXcd> KKinetics;
    std::vector<Eigen::MatrixXcd> KPotentials;
    std::vector<Eigen::MatrixXcd> KHcore;

    KOverlaps.resize(K_L);
    KKinetics.resize(K_L);
    KPotentials.resize(K_L);
    KHcore.resize(K_L);
    AssignZeroList(KOverlaps, nao);
    AssignZeroList(KKinetics, nao);
    AssignZeroList(KPotentials, nao);
    AssignZeroList(KHcore, nao);
 


    double a = double(TV[2]);
    std::vector<double> momentum = { 0.0, 0.0, 0.0};
    std::vector<double> kvec;
    kvec.resize(K_L);
    for(auto i = 0; i != K_L; i++){
        momentum[2] = (2*M_PI/a)*(double(i)/double(K_L));
        kvec[i] = momentum[2];
        KOverlaps[i] = one_fourier(Overlaps,momentum,translateS, a);     
        KKinetics[i] = one_fourier(Kinetic,momentum,translateS, a);
        KPotentials[i] = one_fourier( tVV, momentum, translateS, a);
    }
 
    
  //Initialize vectors of matrices for calculation
  std::vector<Eigen::MatrixXcd> xD; xD.resize(2*NAMUR_S + 1);
  AssignZeroList(xD, nao);
  std::vector<Eigen::MatrixXcd> KDensity; KDensity.resize(K_L);
  AssignZeroList(KDensity, nao);
  std::vector<Eigen::MatrixXcd> KDensity_last; KDensity_last.resize(K_L);
  AssignZeroList(KDensity_last, nao);
  std::vector<Eigen::MatrixXcd> KF_prime; KF_prime.resize(K_L);
  AssignZeroList(KF_prime, nao);
  Eigen::MatrixXcd C;
  std::complex<double> norm;

  if (basis_set.compare("sto-3g") == 0 || 
        guess_density.compare("CORE") == 0) {

        printf("\n\tInitial Density matrix generated from Core\n");

      /*** =========================== ***/
      /*** Generate Guess from Core    ***/
      /*** Matrix                      ***/
      /*** =========================== ***/
  
      for(auto i = 0; i < K_L; i++){
       KF_prime[i] = KKinetics[i]+KPotentials[i];
 
      }
      for(int i = 0; i < K_L; ++i){
         Eigen::GeneralizedSelfAdjointEigenSolver<Eigen::MatrixXcd> diag(KF_prime[i], KOverlaps[i]);
         auto eps = diag.eigenvalues();
         auto C = diag.eigenvectors();         
         auto C_occ = C.leftCols(nocc);
         KDensity[i] = 2 * C_occ * C_occ.adjoint();
      }
    
      for(int i = 0; i < translateS.size(); i++){
          xD[i] = one_fourier_inv(KDensity, kvec, double(translateS[i]), a, nao);
      }
  
      //Norm Check
      std::complex<double> norm = 0;
      for(auto i = 0; i < 2*NAMUR_S + 1; i++){
          norm += (xD[i]*Overlaps[i]).trace();
      }
      printf("\txD block AO Norm = %f+i%f\n",norm.real(), norm.imag());
  
      //Compute Core energy
      double energy = 0;
      for(auto i = 0; i < 2*NAMUR_S + 1; i++){
          energy += ((Kinetic[i] + Potential[i])*xD[i].real()).trace();
      }
      printf("\tCore energy = %f\n", energy);

    } 
    if (guess_density.compare("SOAD") == 0) {
    //produce guess zero matrix
    printf("\n\tInitial Density Matrix From SOAD\n");
    //printf("\nNot supported rerun with CORE for initial density matrix\n");
    printf("\nExperimental!!!!!!!!!!\n");

    std::string guess_file = "guess.rdm";
    ReadGuessDensity(guess_file,nao, xD); 
    //soad(basis_set, atoms, TV, translateS, translateL, xD);
    //transform xD to get density
    for(auto i = 0; i != K_L; i++){
          momentum[2] = (2*M_PI/a)*(double(i)/double(K_L));
          KDensity[i] = one_fourier(xD, momentum, translateS, a);     
     }

    std::cout << xD[0] << std::endl;

    } //close if-else for guess density matrix

    //check norm and reconstruction ordering of density matrix
    //double enorm = 0.0;
    //for(int i = 0; i < 2*NAMUR_S+1; i++){
    //    enorm += ((xD[i])*Overlaps[i]).trace().real();
    //}
    //std::cout << enorm << std::endl;
    //exit(0);

    const auto maxiter = 10000;
    const auto conv = 1e-5;
    auto iter = 0;
    auto rms = 0.0;
    auto ediff = 0.0;
    auto ehf = 0.0;  //energy;
    auto ehf_last = 0.0;
    auto oneE = 0.0;
    auto twoE = 0.0;
    auto FockMultipole = 0.0;
    int DIIS_E_len = 4;
    bool DIIS_ON{false};
    //make a deque of vectors for DIIS interpolation at each k-point
    std::deque<std::vector<Eigen::MatrixXcd>> error;
    std::deque<std::vector<Eigen::MatrixXcd>> fock_q;

    Matrix KpointEigs(nao, K_L);
    std::vector<Eigen::MatrixXcd> xD_last; xD_last.resize(xD.size());
    std::vector<Eigen::MatrixXcd> Coeffs; Coeffs.resize(K_L);
    AssignZeroList(Coeffs, nao);
  
    for(auto i = 0; i < 2*NAMUR_S+1; ++i){
      xD_last[i] = xD[i];
    }
    for(auto i = 0; i != K_L; ++i){
      KDensity_last[i] = KDensity[i];
    }

    //Get matrices ready for SCF procedure
    std::vector<Matrix> Fprime; Fprime.resize(2*NAMUR_S+1);
    AssignZeroList(Fprime, nao);
    std::vector<Matrix> MultiP; MultiP.resize(2*NAMUR_S+1);
    AssignZeroList(MultiP, nao);
    std::vector<Matrix> MultiP2; MultiP2.resize(2*NAMUR_S+1);
    AssignZeroList(MultiP2, nao);
    std::vector<Eigen::MatrixXcd> KF; KF.resize(K_L);
    AssignZeroList(KF, nao);
    std::vector<Eigen::MatrixXcd> Gmat; Gmat.resize(2*NAMUR_S+1);
    AssignZeroList( Gmat, nao);
    std::vector<Eigen::MatrixXcd> Emat; Emat.resize(2*NAMUR_S+1);
    AssignZeroList( Emat, nao);
    std::vector<Eigen::MatrixXcd> J2; J2.resize(2*NAMUR_S+1);
    AssignZeroList( J2, nao);
    std::vector<Eigen::MatrixXcd> X2; X2.resize(2*NAMUR_S+1);
    AssignZeroList( X2, nao);
    std::vector<Eigen::MatrixXcd> kX2; kX2.resize(K_L);
    AssignZeroList( kX2, nao);
    std::vector<Eigen::MatrixXcd> kG; kG.resize(K_L);
    AssignZeroList( kG, nao);
    std::vector<Eigen::MatrixXcd> kJ2; kJ2.resize(K_L);
    AssignZeroList( kJ2, nao);
    std::vector<Eigen::MatrixXcd> KMultiP; KMultiP.resize(K_L);
    AssignZeroList( KMultiP, nao);



    double Ecoulomb = 0.0;
    double Ecoulomb_nuclear = 0.0;
    double Ecoulomb_electronic = 0.0;
    double Eexchange = 0.0;
    double EMPE = 0.0;

    do {
      const auto tstart = std::chrono::high_resolution_clock::now();

      ZeroList(Gmat);      
      ZeroList(J2);
      ZeroList(X2);
      //ZeroList(KDensity);
      ZeroList(MultiP);
      ZeroList(MultiP2);
      ZeroList(KF_prime);
      ZeroList(Fprime);
      ZeroList(KF);
      ZeroMatrix(C);
      ZeroList(Coeffs);
      ZeroMatrix(KpointEigs);
      ZeroList(kX2);
      ZeroList(kG);
      ZeroList(KMultiP);
 
 
   
      ConstructG_Namur_2(xD, translateS, Gmat, J2, X2, nao, K_L, TV, obs, atoms);
      ConstructMultipoleHirata( MultiP,  xD, Overlaps, XDipole, YDipole, 
                ZDipole, XXQuad, YYQuad, ZZQuad,
                XYQuad, XZQuad, YZQuad, atoms, nao, a, TV);
      EMPE =  EMultipoleHirata( xD, Overlaps, XDipole, YDipole, 
              ZDipole, XXQuad, YYQuad, ZZQuad, XYQuad, XZQuad, YZQuad, atoms,
              nao, a, TV);

      // Change to K-space form
      // real space.  Enforce symmetry F_{pq}^{j} = F_{qp}^{-j}
      for(auto i = 0; i < NAMUR_S+1; i++){
        Fprime[i] = Kinetic[i] + Potential[i] + Gmat[i].real() + MultiP[i]  ;
        Fprime[2*NAMUR_S - i] = (Kinetic[i]+Potential[i]+Gmat[i].real()+MultiP[i]).transpose();
      }

      //Fourier Transform for diagonalization
      for(auto i = 0; i < K_L; i++){
          momentum[2] = (2*M_PI/a)*(double(i)/double(K_L));
          KF_prime[i] = one_fourier(Fprime, momentum, translateS, a);     
          kX2[i] = one_fourier(X2, momentum, translateS, a); 
          KMultiP[i] = one_fourier(MultiP, momentum, translateS, a);
      }

      /*************************************
       *  
       *  DIIS ROUTINE
       *  Perform DIIS interpolation on 
       *  k = 0 density matrix
       *
       ************************************/

      if (iter > 0 && DIIS_ON){
          //std::cout << "DIIS ON." << std::endl;
          if(error.size() < DIIS_E_len ){

              //construct Fock error vector
              std::vector<Eigen::MatrixXcd> tmp_error; tmp_error.resize(K_L);
              std::vector<Eigen::MatrixXcd> tmp_fock_q; tmp_fock_q.resize(K_L);
              for(int kk = 0; kk < K_L; kk++){
                  tmp_error[kk] =  KF_prime[kk]*KDensity[kk]*KOverlaps[kk] - KOverlaps[kk]*KDensity[kk]*KF_prime[kk]   ;
                  tmp_fock_q[kk] = KF_prime[kk] ;
              }
              error.push_back(tmp_error);
              fock_q.push_back(tmp_fock_q);
           
              //std::cout << "error vector size " << error.size() << std::endl;
              //std::cout << "fock_q vector size " << fock_q.size() << std::endl;
 
          }

          if (error.size() >= DIIS_E_len ) {

              error.pop_front();
              fock_q.pop_front();

              //construct Fock error vector
              std::vector<Eigen::MatrixXcd> tmp_error; tmp_error.resize(K_L);
              std::vector<Eigen::MatrixXcd> tmp_fock_q; tmp_fock_q.resize(K_L);
              for(int kk = 0; kk < K_L; kk++){
                  tmp_error[kk] =  KF_prime[kk]*KDensity[kk]*KOverlaps[kk] - KOverlaps[kk]*KDensity[kk]*KF_prime[kk]   ;
                  tmp_fock_q[kk] = KF_prime[kk] ;
              }
              error.push_back(tmp_error);
              fock_q.push_back(tmp_fock_q);

              //std::cout << "error vector size " << error.size() << std::endl;
              //std::cout << "fock_q vector size " << fock_q.size() << std::endl;
 

          }
      }
      
      if (error.size() >= 2){

        //Construct Lagrangian and diagonalize at each k-point
        for(int kk = 0; kk < K_L; kk++){

          Eigen::MatrixXcd B = Eigen::MatrixXcd::Zero(error.size() + 1, error.size() + 1 );
          Eigen::VectorXcd b = Eigen::VectorXcd::Zero(B.rows());
          for(int ii = 0; ii < error.size(); ii++) { B(0,ii+1) = -1; B(ii+1,0) = -1; }
          b(0) = std::complex<double> (-1.0,0);
          for(int erri = 0; erri < error.size(); erri++){
            for(int errj = 0; errj < error.size(); errj++){
                B(erri + 1, errj + 1) = (error[erri][kk]*(error[errj][kk].transpose()) ).trace();
            }//errj
          }//erri

          Eigen::VectorXcd coeffs = B.fullPivHouseholderQr().solve(b);
          //set our old matrix to zero
          KF_prime[kk] = Eigen::MatrixXcd::Zero(KF_prime[kk].rows(), KF_prime[kk].cols());
          for(int ii = 0; ii < fock_q.size(); ii++){
            KF_prime[kk] += coeffs(ii+1)*fock_q[ii][kk];
          }
 
        }//kk
        
        
       
      }//error.size() >= 2
     
      /*************************************
      *  
      *  DIAGONALIZE FOCK OPERATOR
      *
      *  *********************************/
      ZeroList(KDensity);
      for(auto i = 0; i != K_L; i++){
          Eigen::GeneralizedSelfAdjointEigenSolver<Eigen::MatrixXcd> diag(KF_prime[i], KOverlaps[i]);
          auto eps = diag.eigenvalues();
          auto C = diag.eigenvectors();
          Coeffs[i] = C;
          auto C_occ = C.leftCols(nocc);
          KDensity[i] = 2 * C_occ * C_occ.adjoint();
          KpointEigs.block(0,i,nao,1) = eps.leftCols(1);
      }  
  
    //Provide some damping...should try without it and see how it goes
      if (iter > 0 && !DIIS_ON ){
          //std::cout << "DIIS NOT ON. USING DAMPING INSTEAD" << std::endl;
        for(int i = 0; i != K_L; i++){
            KDensity[i] = 0.5*KDensity[i] + (1.0 - 0.5)*KDensity_last[i] ;
        }
      } 
      ZeroList(xD);
      for(int i = 0; i < translateS.size(); i++){
          xD[i] = one_fourier_inv(KDensity, kvec, double(translateS[i]), a, nao);
      }

      // compute HF energy
      ehf = 0.0;
      oneE = 0.0;
      twoE = 0.0;
      FockMultipole = 0.0;
      for(auto l = 0; l < xD.size(); l++){
        oneE += (xD[l]*(Kinetic[l]+Potential[l] + MultiP[l])).trace().real();     
        FockMultipole += (xD[l]*MultiP[l]).trace().real(); 
        twoE += ((0.5*xD[l])*Gmat[l].real()).trace().real();
        for (auto i = 0; i < nao; i++){
          for (auto j = 0; j < nao; j++){
            ehf += 0.5*xD[l](j,i).real() * (2*(Kinetic[l] + Potential[l] + MultiP[l]  ) + Gmat[l].real()    )(i,j) ;
 
          }
        }
      }

      //check if we added energies together correctly
      //#std::cout << oneE + twoE + enuc + EMPE << "\t" << ehf + enuc + EMPE << std::endl;

      norm = 0;
      for(auto i = 0; i < 2*NAMUR_S + 1; i++){
          norm += (xD[i]*Overlaps[i]).trace();
      }

      
      // compute difference with last iteration
      ediff = ehf - ehf_last;
      rms = 0.0;
      for(auto i = 0; i < xD.size(); i++){
          rms += (xD_last[i].real() - xD[i].real()).norm();
      }
       
      const auto tstop = std::chrono::high_resolution_clock::now();
      const std::chrono::duration<double> time_elapsed = tstop - tstart;

      if (iter == 0)
        std::cout <<
        "\n\n Iter        1E(elec)              2E(elec)               (E(tot))             RMS(D)         Time(s)      Norm\n";
      printf(" %02d %20.12f %20.12f %20.12f %20.12e %10.5lf %10.5f\n", iter, oneE, twoE,
             oneE + twoE + enuc + EMPE, rms, time_elapsed.count(), norm.real());
     
       // Save a copy of the energy and the density
      for(auto i = 0; i < 2*NAMUR_S+1;i++){
          xD_last[i] = xD[i];
      }
      for(auto i = 0; i != K_L;i++){
          KDensity_last[i] = KDensity[i];
      }

      ehf_last = ehf;
      Ecoulomb = 0.0;
      Ecoulomb_nuclear = 0.0;
      Ecoulomb_electronic = 0.0;
      for(int i = 0; i < 2*NAMUR_S+1; i++){
        Ecoulomb += 0.5*(xD[i]*J2[i]).trace().real();
        Ecoulomb +=  (xD[i]*Potential[i]).trace().real();
        Ecoulomb_electronic += 0.5*(xD[i]*J2[i]).trace().real();
        Ecoulomb_nuclear +=  (xD[i]*Potential[i]).trace().real();
 
      }
      Ecoulomb += enuc;

      Eexchange = 0.0;
      for(int i = 0; i < 2*NAMUR_S+1; i++){
        Eexchange += 0.5*(xD[i]*X2[i]).trace().real();
      }
 

    ++iter;
    } while (((fabs(ediff) > conv) || (fabs(rms) > conv)) && (iter < maxiter));



    printf("** Hartree-Fock energy = %20.12f\n", oneE+twoE+enuc +EMPE);
 
    libint2::cleanup(); // done with libint

    printf("\tTotal Coulomb Energy %20.12f\n", Ecoulomb);
    printf("\tExchange energy %20.12f\n", Eexchange);
    printf("\tCoulomb Nuclear %20.12f\n", Ecoulomb_nuclear);
    printf("\tCoulomb Electronic %20.12f\n", Ecoulomb_electronic);
    printf("\tDipole Multipole Energy Correction %20.12f\n", EMPE);
    printf("\tFirst Order Fock Multipole Energy Correction %20.12f\n", FockMultipole);



    //std::cout << "\nxD[center].real should be symmetric\n\n";
    //std::cout << xD[NAMUR_S].real() << std::endl;
    //std::cout << "\nxD[center].imag should be anti and zero\n\n";
    //std::cout << "\n" << xD[NAMUR_S].imag() << std::endl;
    std::cout << "\nxD[boundary].real should be zero\n\n";
    std::cout << xD[0].real().norm() << std::endl;
    std::cout << "\nxD[boundary].imag should be zero\n\n";
    std::cout << "\n" << xD[0].imag().norm() << "\n\n" << std::endl;

  std::cout.precision(15);
  std::ofstream ofs;
  ofs.precision(15);
  ofs.open ("xAO.rdm", std::ofstream::out );
    //printf("\n\n\tReal space density matrices\n");
    ofs << "\n\n\tReal space density matrices\n";
    for(auto i = 0; i < translateS.size(); i++){
        ofs << "\tcell = " << translateS[i] << "\n";
        ofs << xD[i].real().format(HeavyFmt) << "\n\n";
 
    }
  ofs.close();

  ofs.open ("guess.rdm", std::ofstream::out );
    //printf("\n\n\tReal space density matrices\n");
    for(int i = 0; i < 2*NAMUR_S + 1; i++){
        for(int x = 0; x < nao; x++){
            for(int y = 0; y < nao; y++){
                ofs << xD[i](x,y).real() << "\n";
            }
        }
    }
  ofs.close();

  std::cout.precision(15);
  ofs.precision(15);
  ofs.open ("band.dat", std::ofstream::out );

    ofs << "\n\ta = " << a << "\n"; 
    ofs << "\n\tBand Structure\n";
    //printf("\n\n\tBand Structure\n");
    for(auto i = 0; i < K_L; i++){
        ofs << "\tk = " << (2.*M_PI/a)*(double(i)/double(K_L))<< "\n";
        ofs << KpointEigs.block(0,i,nao,1) << "\n\n";
 
    }
  ofs.close();

  ofs.precision(15);
  for(int i = 0; i < K_L; i++){
    ofs.open ("Coeffs" + std::to_string(i) + ".dat", std::ofstream::out );
    ofs << Coeffs[i].cols() << "\n"; 
    for(int j = 0; j < Coeffs[i].cols(); j++){
        ofs << "col = " << j << "\n";
        for(auto n = 0; n < nao; n++){
            ofs << Coeffs[i](j,n) << "\n";
        }
    }
    ofs.close();
  }



  if (PRINT == 2){
      MOIntegrals(KpointEigs, xD, KDensity, KKinetics, KPotentials, KOverlaps, Coeffs, 
              kvec, nao, a, enuc, obs, atoms, TV, KMultiP, EMPE );



  }//end of PRINT level if



  } // end of try block; if any exceptions occurred, report them and exit cleanly

  catch (const char* ex) {
    cerr << "caught exception: " << ex << endl;
    return 1;
  }
  catch (std::string& ex) {
    cerr << "caught exception: " << ex << endl;
    return 1;
  }
  catch (std::exception& ex) {
    cerr << ex.what() << endl;
    return 1;
  }
  catch (...) {
    cerr << "caught unknown exception\n";
    return 1;
  }

  return 0;



}


std::vector<Atom> read_geometry(const std::string& filename, std::vector<double>& TV, std::string& cutoff_type) {

  std::cout << "\tWill read geometry from " << filename << std::endl;
  std::ifstream is(filename);
  assert(is.good());

  // to prepare for MPI parallelization, we will read the entire file into a string that can be
  // broadcast to everyone, then converted to an std::istringstream object that can be used just like std::ifstream
  std::ostringstream oss;
  oss << is.rdbuf();
  // use ss.str() to get the entire contents of the file as an std::string
  // broadcast
  // then make an std::istringstream in each process
  std::istringstream iss(oss.str());

  // check the extension: if .xyz, assume the standard XYZ format, otherwise throw an exception
  if ( filename.rfind(".xyz") != std::string::npos) {
    std::vector<Atom> atoms = libint2::read_dotxyz(iss);
    

    double x,y,z, lr;
    std::string comment;
 
    std::getline(iss, comment);
    std::string TV_label;
    iss >> TV_label >> x >> y >> z;
    printf("\n\tTranslational Vector\n");
    printf("\n\t%s\t%f\t%f\t%f\n",TV_label.c_str(), x,y,z);

    TV[0] = x;
    TV[1] = y;
    TV[2] = z;

    std::getline(iss, comment);
    std::string N_L;
    iss >> N_L >> x;
    NAMUR_L = int(x);

    std::string N_S;
    iss >> N_S >> x;
    NAMUR_S = int(x);

    printf("\n\tNAMUR_L = %i\n",NAMUR_L);
    printf("\n\tNAMUR_S = %i\n",NAMUR_S);

    std::string CT_tmp;
    iss >> CT_tmp;
    cutoff_type.assign(CT_tmp);

    iss >> N_L >> K_L;//use previous string to catch comment

    std::string basis_tmp;
    iss >> basis_tmp;

    basis_set.assign(basis_tmp);

    std::getline(iss, comment);
    std::string print_level;
    iss >> print_level >> x;
    PRINT = int(x);
    
    printf("\n\tPRINT LEVEL = %i\n",PRINT);

    std::string g_density;
    iss >> g_density;
    guess_density.assign(g_density);

    printf("\n\tGuess Density Method = %s\n", guess_density.c_str());

    iss >> comment >> lr;
    LR = lr;
  
    return atoms;

    //return read_dotxyz(iss, TV, cutoff_type);
  } else {
    throw "only .xyz files are accepted";
  }
}

template <libint2::OneBodyEngine::operator_type obtype>
std::array<Matrix, libint2::OneBodyEngine::operator_traits<obtype>::nopers>
compute_1body_ints(BasisSet& obs,
                          std::vector<double>& TV,
                          int n_trans,
                          const std::vector<Atom>& atoms){

  BasisSet shells_shift = obs;


  //SHIFT OVER CELLs 
  if (n_trans != 0){
    for(auto i = 0; i < shells_shift.size(); i++){
      //shells_shift[i].O[0] += n_trans*TV[0];
      //shells_shift[i].O[1] += n_trans*TV[1];
      shells_shift[i].O[2] += n_trans*TV[2];
    }
  }


  const auto n = obs.nbf();

#ifdef _OPENMP
  const auto nthreads = 1;//Set to 1 regardless//omp_get_max_threads();
#else
  const auto nthreads = 1;
#endif
  typedef std::array<Matrix, libint2::OneBodyEngine::operator_traits<obtype>::nopers> result_type;
  const unsigned int nopers = libint2::OneBodyEngine::operator_traits<obtype>::nopers;
  result_type result; for(auto& r: result) r = Matrix::Zero(n,n);


  std::vector<libint2::OneBodyEngine> engines(nthreads); //one thread
  engines[0] = libint2::OneBodyEngine(obtype, obs.max_nprim(), obs.max_l(), 0);
  if (obtype == libint2::OneBodyEngine::nuclear) {
    std::vector<std::pair<double,std::array<double,3>>> q;

    std::vector<int> translateL(2*NAMUR_L + 1);//NAMUR Cut uses long cut off
    std::iota(std::begin(translateL), std::end(translateL), -1*NAMUR_L);
    std::vector<int> translateS(2*NAMUR_S + 1);//NAMUR Cut uses long cut off
    std::iota(std::begin(translateS), std::end(translateS), -1*NAMUR_S);


    double tmp_atom_x;
    double tmp_atom_y;
    double tmp_atom_z;
  
    for (auto i = 0; i < translateL.size(); i++){

      for(const auto& atom : atoms) {

         tmp_atom_x = atom.x + translateL[i]*TV[0];
         tmp_atom_y = atom.y + translateL[i]*TV[1];
         tmp_atom_z = atom.z + translateL[i]*TV[2];

          q.push_back( {static_cast<double>(atom.atomic_number), 
                    {{tmp_atom_x, tmp_atom_y, tmp_atom_z}}} );
     
            }
        }
    engines[0].set_params(q);

  }

  auto shell2bf = obs.shell2bf();
  auto shell2bf_shift = obs.shell2bf();


  for(auto s1=0; s1!=obs.size(); ++s1) {

    auto bf1 = shell2bf[s1]; // first basis function in this shell
    auto n1 = obs[s1].size();

    for(auto s2=0; s2!=shells_shift.size(); ++s2) {

      auto bf2 = shell2bf[s2];
      auto n2 = shells_shift[s2].size();
      
      auto n12 = n1 * n2;
    
      const auto* buf = engines[0].compute( obs[s1], shells_shift[s2]);

      for(unsigned int op=0; op!=nopers; ++op, buf+=n12) {
        Eigen::Map<const Matrix> buf_mat(buf, n1, n2);
        result[op].block(bf1, bf2, n1, n2) = buf_mat;
      }
    }
  }

  return result;
}


Matrix compute_2body_ints(BasisSet& shells, const int m1_cell, 
        const int m2_cell, const int m3_cell, const int nao, std::vector<double>& TV, std::vector<Atom>& atoms){

    //make three copies of shell.  Because they are now objects we must clone
    //them
  BasisSet shells_m1 = shells;
  //SHIFT OVER CELLs 
  if (m1_cell != 0){
    for(auto i = 0; i < shells_m1.size(); i++){
      shells_m1[i].O[0] += m1_cell*TV[0];
      shells_m1[i].O[1] += m1_cell*TV[1];
      shells_m1[i].O[2] += m1_cell*TV[2];
    }
  }
  BasisSet shells_m2 = shells;
  //SHIFT OVER CELLs 
  if (m2_cell != 0){
    for(auto i = 0; i < shells_m2.size(); i++){
      shells_m2[i].O[0] += m2_cell*TV[0];
      shells_m2[i].O[1] += m2_cell*TV[1];
      shells_m2[i].O[2] += m2_cell*TV[2];
    }
  }
  BasisSet shells_m3 = shells;
  //SHIFT OVER CELLs 
  if (m3_cell != 0){
    for(auto i = 0; i < shells_m3.size(); i++){
      shells_m3[i].O[0] += m3_cell*TV[0];
      shells_m3[i].O[1] += m3_cell*TV[1];
      shells_m3[i].O[2] += m3_cell*TV[2];
    }
  }



  const auto n = shells.nbf();
  Matrix VV  = Matrix::Zero(nao*nao, nao*nao);


// construct the electron repulsion integrals engine
  typedef libint2::TwoBodyEngine<libint2::Coulomb> coulomb_engine_type;
  std::vector<coulomb_engine_type> engines(1); //1 thread
  engines[0] = coulomb_engine_type(shells.max_nprim(), shells.max_l(), 0);
  

  engines[0].set_precision(std::numeric_limits<double>::epsilon());

  //libint2::TwoBodyEngine<libint2::Coulomb> engine(shells.max_nprim(), shells.max_l(), 0);

  auto shell2bf = shells.shell2bf();
  auto shell2bf_m1 = shells_m1.shell2bf();
  auto shell2bf_m2 = shells_m2.shell2bf();
  auto shell2bf_m3 = shells_m3.shell2bf();


  //shell integrals look like (ab|cd) where ab correspond to distribution for
  //electron 1 and cd correspond to distriution for electrion 2.  To put into
  //physics notation we must do <a,d|b,c>
  for(auto s1=0; s1!=shells.size(); ++s1) {

    auto bf1_first = shell2bf[s1]; // first basis function in this shell
    auto n1 = shells[s1].size();

    for(auto s2=0; s2!=shells_m1.size(); ++s2) {


      auto bf2_first = shell2bf_m1[s2];
      auto n2 = shells_m1[s2].size();

      for(auto s3=0; s3!=shells_m2.size(); ++s3) {

        auto bf3_first = shell2bf_m2[s3];
        auto n3 = shells_m2[s3].size();

        for(auto s4=0; s4!=shells_m3.size(); ++s4) {

          auto bf4_first = shell2bf_m3[s4];
          auto n4 = shells_m3[s4].size();


          const auto* buf_1234 = engines[0].compute(shells[s1], shells_m1[s2], shells_m2[s3], shells_m3[s4] );

        //loop over all integrals in shell set and extract 
          for(auto f1=0, f1234=0; f1!=n1; ++f1) {
            const auto bf1 = f1 + bf1_first;
            for(auto f2=0; f2!=n2; ++f2) {
              const auto bf2 = f2 + bf2_first;
              for(auto f3=0; f3!=n3; ++f3) {
                const auto bf3 = f3 + bf3_first;
                for(auto f4=0; f4!=n4; ++f4, ++f1234) {
                  const auto bf4 = f4 + bf4_first;

                   //store in physics notation 
                  VV(bf1*n+bf3, bf2*n+bf4) = buf_1234[f1234];
                  //printf("<%i,%i|%i,%i>\n",int(bf1),int(bf3),int(bf2),int(bf4));


                }
              }
            }
          }



        }
      }
    }
  }


  return VV;

}


