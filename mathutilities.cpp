/*
 * various utilities required for integrals.cpp
 * templated functions don't allow prototyping so defining here. 
 *
 */

#include "mathutilities.hpp"

std::tuple<int, int> geminal_make(const int i, const int j){
    return std::make_tuple(i,j); 
}

void ZeroMatrix(Matrix& mats)
{
    for(int i = 0; i < mats.rows(); ++i){
        for(int j = 0; j < mats.cols(); ++j){
            mats(i,j) = 0.0;
        }
    }
}
void ZeroMatrix(Eigen::MatrixXcd& mats)
{
    std::complex<double> zero(0,0);
    for(int i = 0; i < mats.rows(); ++i){
        for(int j = 0; j < mats.cols(); ++j){
            mats(i,j) = zero;
        }
    }
}

void ZeroList(std::vector<Matrix>& mats)
{
    for(int i = 0; i < mats.size(); ++i){
        for(int x = 0; x < mats[i].rows(); ++x){
            for(int y = 0; y < mats[i].cols(); ++y){
                mats[i](x,y) = 0.0;
            }
        }
    }
}

void ZeroList(std::vector<Eigen::MatrixXcd>& mats)
{
    for(int i = 0; i < mats.size(); ++i){
      ZeroMatrix(mats[i]);
    }
}
void AssignZeroList(std::vector<Matrix>& mats, const int n)
{
    for(int i = 0; i < mats.size(); ++i){
        mats[i] = Matrix::Zero(n,n);
    }
}

void AssignZeroList(std::vector<Eigen::MatrixXcd>& mats, const int n)
{
    for(int i = 0; i < mats.size(); ++i){
        mats[i] = Eigen::MatrixXcd::Zero(n,n);
    }
}

void printMats(std::vector<Matrix>& mats)
{
  for(auto i(0); i < mats.size(); i++){
    std::cout << mats[i] << "\n" << std::endl;
  }
}

Eigen::MatrixXcd one_fourier(const std::vector<Matrix>& cells, 
        std::vector<double>& K, const std::vector<int>& translate,
        const double a)
{

  std::complex<double> ii(0,1);
  std::complex<double> nii(0,-1);

  Eigen::MatrixXcd Kmat = Eigen::MatrixXcd::Zero(cells[0].rows(), cells[0].cols()); 

  for (int m = 0; m < translate.size(); m++){
      Kmat += cells[m]*exp(ii*K[2]*double(a)*double(translate[m])); 
      

  }

  return Kmat;
    
}
Eigen::MatrixXcd one_fourier(const std::vector<Eigen::MatrixXcd>& cells, 
        std::vector<double>& K, const std::vector<int>& translate,
        const double a)
{

  std::complex<double> ii(0,1);
  std::complex<double> nii(0,-1);

  Eigen::MatrixXcd Kmat = Eigen::MatrixXcd::Zero(cells[0].rows(), cells[0].cols()); 
    
  for (int m = 0; m < translate.size(); m++){
      Kmat += cells[m]*exp(ii*K[2]*double(a)*double(translate[m])); 
     }

  return Kmat;
    
}

Eigen::MatrixXcd one_fourier_inv(const std::vector<Eigen::MatrixXcd>& kcells, 
        std::vector<double>& kvec, const double xpos,
        const double a, const int nao)
{

  std::complex<double> ii(0,1);
  Eigen::MatrixXcd tmp = Eigen::MatrixXcd::Zero( nao, nao );
  for(int k = 0; k < K_L; ++k){
    tmp += kcells[k]*exp(ii*kvec[k]*a*xpos);
  }
  return tmp/double(K_L);
      
    
}

CSpMat Kronecker(CSpMat& mat1, CSpMat& mat2){
    
  CSpMat m3( mat1.rows()*mat2.rows(), mat1.cols()*mat2.cols());
  std::vector<TC> tripletList;
  for(int k = 0; k < mat1.outerSize(); ++k){
      for(CSpMat::InnerIterator it(mat1,k); it; ++it){

          for(int k2=0; k2<mat2.outerSize(); ++k2){
              for(CSpMat::InnerIterator it2(mat2,k2); it2; ++it2){

                    //it.row(); it.col(); it.value();
                    //it2.row(); it2.col(); it2.value();
                    tripletList.push_back(TC( it.row()*mat2.rows() + it2.row(), it.col()*mat2.cols() + it2.col(), it.value()*it2.value() ) );

                }
            }
        }
    }

  m3.setFromTriplets(tripletList.begin(), tripletList.end());

  return m3;
}

Matrix Kronecker(Matrix& mat1, Matrix& mat2){
    
  Matrix m3( mat1.rows()*mat2.rows(), mat1.cols()*mat2.cols());
  m3.setZero();

  for(int i = 0; i < mat1.cols(); i++){
      for(int j = 0; j < mat1.rows(); j++){

          m3.block(i*mat2.rows(),  j*mat2.cols(), mat2.rows(), mat2.cols()) = mat1(i,j)*mat2;
      }
  }

  return m3;
}
    
Eigen::MatrixXcd Kronecker(Eigen::MatrixXcd& mat1, Eigen::MatrixXcd& mat2){
    
  Eigen::MatrixXcd m3( mat1.rows()*mat2.rows(), mat1.cols()*mat2.cols());
  m3.setZero();

  for(int i = 0; i < mat1.cols(); i++){
      for(int j = 0; j < mat1.rows(); j++){

          m3.block(i*mat2.rows(),  j*mat2.cols(), mat2.rows(), mat2.cols()) = mat1(i,j)*mat2;
      }
  }

  return m3;
}
//zero out imaginary piece of complex matrix
void ZeroImag(CSpMat& mat){

    for(int k = 0; k < mat.outerSize(); ++k){
        for(CSpMat::InnerIterator it(mat,k); it; ++it){
            it.valueRef() = std::complex<double>(it.value().real(),0);
            //it.value(); //value of element
            //it.row(); //row index of element
            //it.col(); //col index of element
            //it.index(); //index equal to col index
        }
    }

}
void ZeroImag(Eigen::MatrixXcd& mat){

    for(int x = 0; x < mat.rows(); x++){
        for(int y = 0; y < mat.cols(); y++){

            mat(x,y) = std::complex<double>(mat(x,y).real(), 0);
        }
    }

}
