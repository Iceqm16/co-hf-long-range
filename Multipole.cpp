#include "integrals.hpp"
#include "globalvars.hpp"
#include "mathutilities.hpp"

double EMultipoleHirata( const std::vector<Eigen::MatrixXcd>& xD, 
        const std::vector<Matrix>& S, const std::vector<Matrix>& XDipole, 
        const std::vector<Matrix>& YDipole, const std::vector<Matrix>& ZDipole, 
        const std::vector<Matrix>& XXQuad,  const std::vector<Matrix>& YYQuad,
        const std::vector<Matrix>& ZZQuad, const std::vector<Matrix>& XYQuad,
        const std::vector<Matrix>& XZQuad, const std::vector<Matrix>& YZQuad, 
        const std::vector<Atom>& atoms, const int nao, const double a,
        const std::vector<double>& TV){

    /* **********************************
     *
     *   Generate  Energy multipole 
     *         correction
     *
     * *********************************/

    //precompute xD[S]*XDipole[S],  xD[S]*YDipole[S] , xD[S]*ZDipole[S]
    double muX = 0.0; double muY = 0.0; double muZ = 0.0;
    double x_d = 0.0; double y_d = 0.0; double z_d = 0.0;
    //energy correction to be returned 
    double EMPE = 0.0;

    for(int s = 0; s < 2*NAMUR_S + 1; s++){
       muX += (xD[s]*XDipole[s]).trace().real();
       muY += (xD[s]*YDipole[s]).trace().real();
       muZ += (xD[s]*ZDipole[s]).trace().real();
    }
 
    //get total dipole
    for(int atom = 0; atom < atoms.size(); atom++){
        x_d +=  -1*(atoms[atom].x )*atoms[atom].atomic_number;
        y_d +=  -1*(atoms[atom].y )*atoms[atom].atomic_number;
        z_d +=  -1*(atoms[atom].z )*atoms[atom].atomic_number;
    }
    
    double pre = LR;
    EMPE = pre*( pow(muX + x_d,2) + pow(muY + y_d, 2) - 2*pow(muZ + z_d,2)   );

    return EMPE; 

}


void ConstructMultipoleHirata(std::vector<Matrix>& MultipoleCorrect, 
        const std::vector<Eigen::MatrixXcd>& xD, const std::vector<Matrix>& S, const std::vector<Matrix>& XDipole, 
        const std::vector<Matrix>& YDipole, const std::vector<Matrix>& ZDipole, 
        const std::vector<Matrix>& XXQuad,  const std::vector<Matrix>& YYQuad,
        const std::vector<Matrix>& ZZQuad, const std::vector<Matrix>& XYQuad,
        const std::vector<Matrix>& XZQuad, const std::vector<Matrix>& YZQuad, 
        const std::vector<Atom>& atoms, const int nao, const double a,
        const std::vector<double>& TV){

    /* **********************************
     *
     *   Generate multipole correction
     *
     * *********************************/

    //precompute xD[S]*XDipole[S],  xD[S]*YDipole[S] , xD[S]*ZDipole[S]
    double muX = 0.0; double muY = 0.0; double muZ = 0.0;
    double x_d = 0.0; double y_d = 0.0; double z_d = 0.0;
    double xxq = 0.0; double yyq = 0.0; double zzq = 0.0;     double xx_d = 0.0; double yy_d = 0.0; double zz_d = 0.0; 
    double tzmr2 =0.0;
    for(int s = 0; s < 2*NAMUR_S + 1; s++){
       muX += (xD[s]*XDipole[s]).trace().real();
       muY += (xD[s]*YDipole[s]).trace().real();
       muZ += (xD[s]*ZDipole[s]).trace().real();
       xxq += (xD[s]*XXQuad[s]).trace().real();
       yyq += (xD[s]*YYQuad[s]).trace().real();
       zzq += (xD[s]*ZZQuad[s]).trace().real();
       tzmr2 += (xD[s]*( 2*ZZQuad[s] - XXQuad[s] - YYQuad[s] ) ).trace().real();
 
    }

    //get total dipole
    for(int atom = 0; atom < atoms.size(); atom++){
        x_d += -1*(atoms[atom].x )*atoms[atom].atomic_number;
        y_d += -1*(atoms[atom].y )*atoms[atom].atomic_number;
        z_d += -1*(atoms[atom].z )*atoms[atom].atomic_number;
        xx_d += -1*(atoms[atom].x*atoms[atom].x )*atoms[atom].atomic_number;
        yy_d += -1*(atoms[atom].y*atoms[atom].y )*atoms[atom].atomic_number;
        zz_d += -1*(atoms[atom].z*atoms[atom].z )*atoms[atom].atomic_number;
    }

    double pre = LR;
    for(int s = 0; s < 2*NAMUR_S + 1; ++s) {
        MultipoleCorrect[s] = pre*( XDipole[s]*(2*(muX + x_d)) + YDipole[s]*(2*(muY + y_d)) - 4*ZDipole[s]*((muZ + z_d)) ); 
        MultipoleCorrect[s] += pre*( S[s]*( (2*zzq - xxq - yyq) + (2*zz_d - xx_d - yy_d )  ) );

    }
    

}






