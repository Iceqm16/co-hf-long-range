/***
 * This is the header file for integrals.  Contains data structures for
 * sorting and such.  Also contains function declationas for integral program
 * and sub programs
 * */


#ifndef INTEGRALS_HPP
#define INTEGRALS_HPP

//#define EIGEN_USE_MKL_ALL 

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

// standard C++ headers
#include <cmath>
#include <math.h>
#include <complex>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <chrono>
#include <map>
#include <tuple>
#include <deque>
#include <typeinfo>

// Libint Gaussian integrals library
#include <libint2.hpp>

#if defined(_OPENMP) 
# include <omp.h>
#endif

using libint2::Shell;
using libint2::Atom;
using libint2::BasisSet;

 
struct e_ev_pair{

    double kval;
    int idx; //For indexing back into correct momentum block
    double eig;  
    Eigen::MatrixXcd eigvec;

    e_ev_pair(int ii, double k, const double  eigenvalues,
            const Eigen::MatrixXcd& eigenvectors) : kval(k), idx(ii), eig(eigenvalues), eigvec(eigenvectors) {}

    bool operator<( const e_ev_pair& val ) const { 
        return eig < val.eig; 
    }
};

//geminal structure for ordering AO-k matrices
struct gem{
    int x;
    int y;
    int kx;
    int ky;

    gem(int xx, int yy, int kxx, int kyy ) : x(xx), y(yy), kx(kxx), ky(kyy)  {}
};
struct korb{
    int x;
    int kx;

    korb(int xx, int kxx) : x(xx), kx(kxx)  {}
};



//Function Declarations for program
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>
        Matrix;  // import dense, dynamically sized Matrix type from Eigen;
                 // this is a matrix with row-major storage (http://en.wikipedia.org/wiki/Row-major_order)
                 // to meet the layout of the integrals returned by the Libint integral library
typedef Eigen::Matrix<std::complex<double>, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> CMatrix; //import dense, dynamiccally size complex Matrix from Eigen;
typedef Eigen::SparseMatrix<double> SpMat; // declares a column-major sparse matrix type of double
typedef Eigen::SparseMatrix<std::complex<double>> CSpMat; // declares a column-major sparse matrix type of double
typedef Eigen::Triplet<double> T;
typedef Eigen::Triplet<std::complex<double>> TC;


typedef std::pair<std::tuple<int, int>, std::tuple<int,int>> geminal;
typedef std::map<int, geminal> bas_map;
typedef std::map<geminal, int> bas_map_rev;

std::tuple<int, int> geminal_make(const int, const int);

std::vector<Atom> read_geometry(const std::string& filename, std::vector<double>&,std::string&);

template <libint2::OneBodyEngine::operator_type obtype>
std::array<Matrix, libint2::OneBodyEngine::operator_traits<obtype>::nopers>
compute_1body_ints( BasisSet& ,
                          std::vector<double>&,
                          int,
                          const std::vector<Atom>& atoms = std::vector<Atom>()
                          );


void ConstructG(std::vector<Eigen::MatrixXcd>& , std::vector<Matrix>&, std::vector<int>&, 
        std::vector<Eigen::MatrixXcd>& , int , int ); 

void ConstructG2(std::vector<Eigen::MatrixXcd>& , std::vector<Matrix>&, 
        std::vector<int>&, 
        std::vector<Eigen::MatrixXcd>& , int , int ); 

void ConstructG_cellwise(std::vector<Eigen::MatrixXcd>& , std::vector<Matrix>&, 
        std::vector<int>&, 
        std::vector<Eigen::MatrixXcd>& , int , int ); 

void ConstructG_Namur(std::vector<Eigen::MatrixXcd>& , 
        std::vector<int>&, 
        std::vector<Eigen::MatrixXcd>& ,
        std::vector<Eigen::MatrixXcd>& , 
        std::vector<Eigen::MatrixXcd>& ,
        int , int,
        std::vector<double>&,
        BasisSet&,
        std::vector<Atom>&); 

void ConstructG_Namur_2(std::vector<Eigen::MatrixXcd>& , 
        std::vector<int>&, 
        std::vector<Eigen::MatrixXcd>& ,
        std::vector<Eigen::MatrixXcd>& , 
        std::vector<Eigen::MatrixXcd>& ,
        int , int,
        std::vector<double>&,
        BasisSet&,
        std::vector<Atom>&); 

void ConstructMultipoleHirata(std::vector<Matrix>&, const std::vector<Eigen::MatrixXcd>& ,const std::vector<Matrix>&, const std::vector<Matrix>&, 
        const std::vector<Matrix>& , const std::vector<Matrix>& , const std::vector<Matrix>& , 
        const std::vector<Matrix>& , const std::vector<Matrix>& , const std::vector<Matrix>& ,
        const std::vector<Matrix>& , const std::vector<Matrix>& , const std::vector<Atom>&,
        const int, const double,const std::vector<double>&);

double EMultipoleHirata( const std::vector<Eigen::MatrixXcd>& , const std::vector<Matrix>&,  const std::vector<Matrix>&,
        const std::vector<Matrix>& , const std::vector<Matrix>& , const std::vector<Matrix>& , 
        const std::vector<Matrix>& , const std::vector<Matrix>& , const std::vector<Matrix>& ,
        const std::vector<Matrix>& , const std::vector<Matrix>& , const std::vector<Atom>&,
        const int, const double,const std::vector<double>&);


void LongRangeCorrection(BasisSet& , std::vector<Matrix>& , 
        const std::vector<Eigen::MatrixXcd>& , const std::vector<Atom>& , 
        const int, const double ,
        const std::vector<double>&); 

double LongRangeTotal(BasisSet& , std::vector<Matrix>& , 
        const std::vector<Eigen::MatrixXcd>& , const std::vector<Atom>& , 
        const int, const double ,
        const std::vector<double>&); 



template <libint2::OneBodyEngine::operator_type obtype>
std::array<Matrix, libint2::OneBodyEngine::operator_traits<obtype>::nopers>
compute_1body_longrange(BasisSet& , const std::vector<double>& ,
                          const int, const int, const std::vector<Atom>& );

Matrix compute_2body_longrange1(BasisSet& , const int, 
        const int, const int , const int , const std::vector<double>& , const std::vector<Atom>&, int );
Matrix compute_2body_longrange2(BasisSet& , const int, 
        const int, const int , const int , const std::vector<double>& , const std::vector<Atom>&, int );




Matrix compute_2body_ints(BasisSet&, const int,
        const int, const int, const int, std::vector<double>&,
        std::vector<Atom>&);

void rotateAO2KAO2ints(std::vector<Matrix>&, Eigen::MatrixXcd& , std::vector<double>&,
        int, double, int);

void rotateAO2KAO2intsOne( Eigen::MatrixXcd&, std::vector<double>&,
        std::vector<gem>&, int, double, int, double, BasisSet&, std::vector<Atom>&, 
        std::vector<double>&);

void rotateAO2MO2ints(std::vector<Matrix>&, Eigen::MatrixXcd& , std::vector<double>&, 
        std::vector<Eigen::MatrixXcd>&, int, double, int);

void soad(std::string, std::vector<Atom>&,
        std::vector<double>& ,
        std::vector<int>& , std::vector<int>&,
        std::vector<Eigen::MatrixXcd>&);


void MOIntegrals(Matrix& , std::vector<Eigen::MatrixXcd>&, std::vector<Eigen::MatrixXcd>& ,
        std::vector<Eigen::MatrixXcd>&, std::vector<Eigen::MatrixXcd>&,  std::vector<Eigen::MatrixXcd>&,
        std::vector<Eigen::MatrixXcd>& ,  std::vector<double>&, 
        const int, const double, const double,
        BasisSet&, std::vector<Atom>&, std::vector<double>&,
        std::vector<Eigen::MatrixXcd>&,
        double EMPE); 

void MOIntegrals2(Matrix& , std::vector<Eigen::MatrixXcd>&, std::vector<Eigen::MatrixXcd>& ,
        std::vector<Eigen::MatrixXcd>&, std::vector<Eigen::MatrixXcd>&,  std::vector<Eigen::MatrixXcd>&,
        std::vector<Eigen::MatrixXcd>& , std::vector<Matrix>&, std::vector<double>&, 
        const int, const double, const double);

void ReadGuessDensity(std::string, int, std::vector<Eigen::MatrixXcd>&);

#endif /* integrals.hpp */
