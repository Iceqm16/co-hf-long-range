#ifndef MATHUTIL_HPP
#define MATHUTIL_HPP

#include "integrals.hpp"
#include "globalvars.hpp"

void ZeroMatrix(Matrix&);
void ZeroMatrix(Eigen::MatrixXcd&);
void ZeroList(std::vector<Matrix>& );
void printMats(std::vector<Matrix>&);
void AssignZeroList(std::vector<Matrix>& mats, const int n);
void AssignZeroList(std::vector<Eigen::MatrixXcd>& mats, const int n);
Eigen::MatrixXcd one_fourier(const std::vector<Matrix>&, std::vector<double>&,
        const std::vector<int>&, const double a);
Eigen::MatrixXcd one_fourier(const std::vector<Eigen::MatrixXcd>&, std::vector<double>&,
        const std::vector<int>&, const double a);
Matrix Kronecker(Matrix& , Matrix&);
CSpMat Kronecker(CSpMat& , CSpMat&);
Eigen::MatrixXcd Kronecker(Eigen::MatrixXcd& , Eigen::MatrixXcd&);
Eigen::MatrixXcd one_fourier_inv(const std::vector<Eigen::MatrixXcd>&,
        std::vector<double>&, const double,
        const double, const int );
void ZeroList(std::vector<Eigen::MatrixXcd>&);
void ZeroImag(CSpMat&);
void ZeroImag(Eigen::MatrixXcd&);



#endif

